#!/usr/bin/env bash
set -x
. /app/env.sh
mkdir -p ~/.ssh
cp /app/id_rsa ~/.ssh
chmod 0600 ~/.ssh/id_rsa
cp /app/id_rsa.pub ~/.ssh
chmod 0644 ~/.ssh/id_rsa.pub
ssh-keyscan -H -p ${DOKKU_PORT:-22} "${DOKKU_HOST}" >> ~/.ssh/known_hosts
ssh-keyscan -H "gitlab.app42.blue" >> ~/.ssh/known_hosts
npm config set registry ${NPM_REGISTRY}
export NODE_ENV="development"
npm ci --loglevel=error || exit 1
npm run test || exit 1
export NODE_ENV="production"
npm run build || exit 1
#cp report.html /app/dist/report.html # bundle-analyser report
