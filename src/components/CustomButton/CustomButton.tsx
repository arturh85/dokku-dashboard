import * as React from "react";
import {Component} from "react";
import {Button} from "react-bootstrap";
import cx from "classnames";

type CustomButtonProps = {
    fill?: boolean,
    simple?: boolean,
    pullRight?: boolean,
    block?: boolean,
    round?: boolean,
    onClick?: any;
    bsStyle?: any;
    type?: any;
    bsSize?: any;
};

class CustomButton extends Component<CustomButtonProps, {}> {
    render() {
        const {fill, simple, pullRight, round, block, ...rest} = this.props;
        const btnClasses = cx({
            "btn-fill": fill,
            "btn-simple": simple,
            "pull-right": pullRight,
            "btn-block": block,
            "btn-round": round
        });
        return <Button className={btnClasses} {...rest} />;
    }
}

export default CustomButton;
