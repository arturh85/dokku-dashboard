import * as React from "react";
import {Component} from "react";
import {NavLink} from "react-router-dom";
import {connect} from 'react-redux';
import {HeaderLinks} from "../Header/HeaderLinks";
import applicationRoutes from "routes/routes";
import {DokkuState} from "../../reducers/dokku.reducer";
import {AuthenticationState} from "../../reducers/authentication.reducer";
import * as _ from 'lodash'
const imagine = '/images/sidebar-2.jpg';

type SidebarState = {
    width: number
};

type SidebarProps = AuthenticationState & DokkuState & { location?: any }

class Sidebar extends Component<SidebarProps, SidebarState> {
    constructor(props) {
        super(props);
        this.state = {
            width: window.innerWidth
        };
    }

    activeRoute(routeName) {
        return this.props.location.pathname.indexOf(routeName) > -1 ? "active" : "";
    }

    updateDimensions() {
        this.setState({width: window.innerWidth});
    }

    componentDidMount() {
        this.updateDimensions();
        window.addEventListener("resize", this.updateDimensions.bind(this));
    }

    render() {
        const sidebarBackground = {
            backgroundImage: "url(" + imagine + ")"
        };

        const sortedAppNames = Object.keys(this.props.dokkuStatus).sort((a, b) => {
            const aa = !this.props.dokkuStatus[a].appReport;
            const bb = !this.props.dokkuStatus[b].appReport;
            if (aa !== bb) {
                return aa ? -2 : 2;
            }
            return a < b ? -1 : 1;
        });

        return (
            <div
                id="sidebar"
                className="sidebar"
                data-color="black"
                data-image={imagine}
            >
                <div className="sidebar-background" style={sidebarBackground}/>
                <div className="logo">
                    <a
                        href="/"
                        className="simple-text logo-mini"
                    >
                        <div className="logo-img">
                            <img
                                src="/images/logo.png"
                                alt="logo_image"/>
                        </div>
                    </a>
                    <a
                        href="/"
                        className="simple-text logo-normal"
                    >
                        app42.blue apps
                    </a>
                </div>
                <div className="sidebar-wrapper">
                    <ul className="nav">
                        {this.state.width <= 991 ? <HeaderLinks/> : null}
                        {applicationRoutes.map((prop, key) => {
                            if (!prop.redirect && !prop.hideInMenu) {
                                // console.log('----');
                                // console.log(this.props.user.permissions);
                                // console.log(prop.permissionsRequired);
                                if (prop.permissionsRequired) {
                                    // console.log(_.intersection(this.props.user.permissions,
                                    //     prop.permissionsRequired));
                                }
                                if (prop.permissionsRequired && _.intersection(this.props.user.permissions,
                                    prop.permissionsRequired).length != prop.permissionsRequired.length) {
                                    return null;
                                }
                                return (
                                    <li className={this.activeRoute(prop.path)}
                                        key={key}>
                                        <NavLink
                                            to={prop.path}
                                            className="nav-link"
                                            activeClassName="active">
                                            <i className={prop.icon}/>
                                            <p>{prop.name}</p>
                                        </NavLink>
                                    </li>
                                );
                            }
                            return null;
                        })}
                        {sortedAppNames.map((appName, index) => {
                            let icon = 'pe-7s-rocket';
                            let style = {};
                            const appStatus = this.props.dokkuStatus[appName];
                            if (appStatus.appReport && appStatus.appReport.Locked === 'true') {
                                icon = 'pe-7s-lock';
                                style = {backgroundColor: 'red'};
                            } else if (!appStatus.appReport) {
                                icon = 'pe-7s-cloud-upload';
                                style = {backgroundColor: 'green'};
                            } else if (!appStatus.letsEncrypt) {
                                icon = 'pe-7s-alarm';
                                style = {backgroundColor: '#c7918f63'};
                            }
                            return (
                                <li className={this.activeRoute('/app/' + appName)}
                                    key={index}>
                                    <NavLink
                                        style={style}
                                        to={'/app/' + appName}
                                        className="nav-link"
                                        activeClassName="active"
                                    >
                                        <i className={icon}/>
                                        <p className="sidebar-menuentry">{appName}</p>
                                    </NavLink>
                                </li>
                            );
                        })}
                    </ul>
                </div>
            </div>
        );
    }
}


function mapStateToProps(state) {
    return {...state.authentication, ...state.dokku};
}

const connected = connect(mapStateToProps)(Sidebar);
export {connected as Sidebar};
