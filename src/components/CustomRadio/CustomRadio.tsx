import * as React from "react";
import {Component} from "react";

class CustomRadio extends Component<any, {}> {
    render() {
        const {name, value, children, onChange, ...rest} = this.props;
        return (
            <div className="radio">
                <input id={name + '_' + value} name={name} type="radio" onChange={onChange} value={value} {...rest} />
                <label htmlFor={name + '_' + value}>{children}</label>
            </div>
        );
    }
}

export default CustomRadio;
