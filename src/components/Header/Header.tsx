import * as React from "react";
import {Component} from "react";
import {Navbar} from "react-bootstrap";
import {HeaderLinks} from "./HeaderLinks";
import applicationRoutes from "routes/routes";

type HeaderState = {
    sidebarExists: boolean
};

class Header extends Component<{}, HeaderState> {
    constructor(props) {
        super(props);
        this.mobileSidebarToggle = this.mobileSidebarToggle.bind(this);
        this.state = {
            sidebarExists: false
        };
    }

    mobileSidebarToggle(e) {
        if (this.state.sidebarExists === false) {
            this.setState({
                sidebarExists: true
            });
        }
        e.preventDefault();
        document.documentElement.classList.toggle("nav-open");
        var node = document.createElement("div");
        node.id = "bodyClick";
        node.onclick = function () {
            this['parentElement'].removeChild(this);
            document.documentElement.classList.toggle("nav-open");
        };
        document.body.appendChild(node);
    }

    getBrand() {
        const pathname = this.props['location'].pathname;
        var name;
        applicationRoutes.map((prop, key) => {
            if (prop['collapse']) {
                prop['views'].map((prop, key) => {
                    if (prop.path === pathname) {
                        name = prop.name;
                    }
                    return null;
                });
            } else {
                if (prop['redirect']) {
                    if (prop.path === pathname) {
                        name = prop.name;
                    }
                } else {
                    if (prop.path === pathname) {
                        name = prop.name;
                    }
                }
            }
            return null;
        });
        if(!name) {
            const appMarker = '/app/';
            if (pathname.indexOf(appMarker) === 0) {
                return 'App: ' + pathname.substr(appMarker.length);
            }
        }
        return name;
    }

    render() {
        return (
            <Navbar fluid>
                <Navbar.Header>
                    <Navbar.Brand>
                        <a href="#">{this.getBrand()}</a>
                    </Navbar.Brand>
                    <Navbar.Toggle onClick={this.mobileSidebarToggle}/>
                </Navbar.Header>
                <Navbar.Collapse>
                    <HeaderLinks/>
                </Navbar.Collapse>
            </Navbar>
        );
    }
}

export default Header;
