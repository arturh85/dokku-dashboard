import * as React from "react";
import {Component} from "react";
import {MenuItem, Nav, NavDropdown, NavItem} from "react-bootstrap";
import {history} from '../../helpers';
import {connect} from 'react-redux';
import {AuthenticationState} from "../../reducers/authentication.reducer";

type HeaderLinksProps = AuthenticationState & { dispatch: any };

class HeaderLinks extends Component<HeaderLinksProps, {}> {
    logoutConstant = '*logout*';

    changeBranch = (action) => {
        if (action === this.logoutConstant) {
            history.push('/login');
        }
    };

    render() {
        const {user} = this.props;
        return (
            <div>
                <Nav pullRight
                     onSelect={this.changeBranch}>
                    <NavItem eventKey={this.logoutConstant}>
                        {user.username} Logout
                    </NavItem>
                </Nav>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {...state.authentication, ...state.branches};
}

const connected = connect(mapStateToProps)(HeaderLinks);
export {connected as HeaderLinks};
