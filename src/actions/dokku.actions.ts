import {DokkuService} from '../services/dokku.service';
import {DokkuConstants} from '../constants/dokku.constants';
import {history} from '../helpers';
import {GitlabService} from "../services/gitlab.service";

export class DokkuActions {
    static loadDokkuStatus = loadDokkuStatus;
    static loadTemplates = loadTemplates;
    static loadLogs = loadLogs;
    static updateStatus = updateStatus;
    static updateStatusReport = updateStatusReport;
    static unlockApp = unlockApp;
    static addApp = addApp;
    static deleteApp = deleteApp;
    static addDomain = addDomain;
    static letsencrypt = letsencrypt;
    static rebuildApp = rebuildApp;
    static deleteDomain = deleteDomain;
    static addPort = addPortMapping;
    static deletePort = deletePortMapping;
    static addStorageMount = addStorageMount;
    static deleteStorageMount = deleteStorageMount;
}

function loadDokkuStatus() {
    return dispatch => {
        dispatch(request());
        DokkuService.loadDokkuStatus()
            .then(
                response => {
                    dispatch(success(<any>response));
                },
                error => {
                    dispatch(failure(error.toString()));
                }
            );
    };

    function request() {
        return {type: DokkuConstants.LOAD_REQUEST};
    }

    function success(response: any) {
        return {type: DokkuConstants.LOAD_SUCCESS, payload: response};
    }

    function failure(error) {
        return {type: DokkuConstants.LOAD_FAILURE, payload: error};
    }
}

function loadTemplates() {
    return dispatch => {
        dispatch(request());
        GitlabService.loadTemplates()
            .then(
                response => {
                    dispatch(success(<any>response));
                },
                error => {
                    dispatch(failure(error.toString()));
                }
            );
    };

    function request() {
        return {type: DokkuConstants.LOAD_TEMPLATES_REQUEST};
    }

    function success(response: any) {
        return {type: DokkuConstants.LOAD_TEMPLATES_SUCCESS, payload: response};
    }

    function failure(error) {
        return {type: DokkuConstants.LOAD_TEMPLATES_FAILURE, payload: error};
    }
}

function updateStatus(appName: string) {
    return dispatch => {
        dispatch(request(appName));
        DokkuService.updateStatus(appName)
            .then(
                () => {
                    dispatch(success());
                    dispatch(loadDokkuStatus());
                },
                error => {
                    dispatch(failure(error.toString()));
                }
            );
    };

    function request(appName: string) {
        return {type: DokkuConstants.UPDATE_STATUS_REQUEST, payload: appName};
    }

    function success() {
        return {type: DokkuConstants.UPDATE_STATUS_SUCCESS};
    }

    function failure(error) {
        return {type: DokkuConstants.UPDATE_STATUS_FAILURE, payload: error};
    }
}

function loadLogs(appName: string, logLines: number) {
    return dispatch => {
        dispatch(request(appName));
        DokkuService.loadLogs(appName, logLines)
            .then(
                appLogs => {
                    dispatch(success(appLogs));
                },
                error => {
                    dispatch(failure(error.toString()));
                }
            );
    };

    function request(appName: string) {
        return {type: DokkuConstants.LOAD_LOGS_REQUEST, payload: appName};
    }

    function success(appLogs) {
        return {type: DokkuConstants.LOAD_LOGS_SUCCESS, payload: appLogs};
    }

    function failure(error) {
        return {type: DokkuConstants.LOAD_LOGS_FAILURE, payload: error};
    }
}

function updateStatusReport() {
    return dispatch => {
        dispatch(request());
        DokkuService.updateStatusReport()
            .then(
                () => {
                    dispatch(success());
                    dispatch(loadDokkuStatus());
                },
                error => {
                    dispatch(failure(error.toString()));
                }
            );
    };

    function request() {
        return {type: DokkuConstants.UPDATE_STATUS_REPORT_REQUEST};
    }

    function success() {
        return {type: DokkuConstants.UPDATE_STATUS_REPORT_SUCCESS};
    }

    function failure(error) {
        return {type: DokkuConstants.UPDATE_STATUS_REPORT_FAILURE, payload: error};
    }
}

function letsencrypt(appName: string) {
    return dispatch => {
        dispatch(request(appName));
        DokkuService.letsencrypt(appName)
            .then(
                output => {
                    dispatch(success());
                    dispatch(loadDokkuStatus());
                },
                error => {
                    dispatch(failure(error.toString()));
                }
            );
    };

    function request(appName: string) {
        return {type: DokkuConstants.LETSENCRYPT_REQUEST, payload: appName};
    }

    function success() {
        return {type: DokkuConstants.LETSENCRYPT_SUCCESS};
    }

    function failure(error) {
        return {type: DokkuConstants.LETSENCRYPT_FAILURE, payload: error};
    }
}

function addApp(appName: string, tagName: string, portString: string, templateName: string) {
    return dispatch => {
        dispatch(request(appName, tagName, templateName));
        DokkuService.addApp(appName, tagName, templateName)
            .then(
                () => {
                    dispatch(success());
                    dispatch(loadDokkuStatus());
                    if (portString.trim()) {
                        const port = parseInt(portString, 10);
                        dispatch(addPortMapping(appName, 'http:80:' + port));
                    }
                    history.push('/app/' + appName);
                },
                error => {
                    dispatch(failure(error.toString()));
                    dispatch(loadDokkuStatus());
                }
            );
    };

    function request(appName: string, tagName: string, newAppTemplate: string) {
        return {type: DokkuConstants.ADD_APP_REQUEST, payload: appName, tagName, newAppTemplate};
    }

    function success() {
        return {type: DokkuConstants.ADD_APP_SUCCESS};
    }

    function failure(error) {
        return {type: DokkuConstants.ADD_APP_FAILURE, payload: error};
    }
}

function unlockApp(appName: string) {
    return dispatch => {
        dispatch(request(appName));
        DokkuService.unlockApp(appName)
            .then(
                () => {
                    dispatch(success());
                    dispatch(loadDokkuStatus());
                },
                error => {
                    dispatch(failure(error.toString()));
                    dispatch(loadDokkuStatus());
                }
            );
    };

    function request(appName: string) {
        return {type: DokkuConstants.UNLOCK_REQUEST, payload: appName};
    }

    function success() {
        return {type: DokkuConstants.UNLOCK_SUCCESS};
    }

    function failure(error) {
        return {type: DokkuConstants.UNLOCK_FAILURE, payload: error};
    }
}

function rebuildApp(appName: string) {
    return dispatch => {
        dispatch(request(appName));
        DokkuService.rebuildApp(appName)
            .then(
                () => {
                    dispatch(success());
                    dispatch(loadDokkuStatus());
                },
                error => {
                    dispatch(failure(error.toString()));
                    dispatch(loadDokkuStatus());
                }
            );
    };

    function request(appName: string) {
        return {type: DokkuConstants.REBUILD_APP_REQUEST, payload: appName};
    }

    function success() {
        return {type: DokkuConstants.REBUILD_APP_SUCCESS};
    }

    function failure(error) {
        return {type: DokkuConstants.REBUILD_APP_FAILURE, payload: error};
    }
}

function deleteApp(appName: string) {
    return dispatch => {
        dispatch(request(appName));
        DokkuService.deleteApp(appName)
            .then(
                () => {
                    dispatch(success());
                    dispatch(loadDokkuStatus());
                    history.push('/apps');
                },
                error => {
                    dispatch(failure(error.toString()));
                    dispatch(loadDokkuStatus());
                }
            );
    };

    function request(appName: string) {
        return {type: DokkuConstants.DELETE_APP_REQUEST, payload: appName};
    }

    function success() {
        return {type: DokkuConstants.DELETE_APP_SUCCESS};
    }

    function failure(error) {
        return {type: DokkuConstants.DELETE_APP_FAILURE, payload: error};
    }
}

function addDomain(appName: string, domain: string) {
    return dispatch => {
        dispatch(request(appName, domain));
        DokkuService.addDomain(appName, domain)
            .then(
                () => {
                    dispatch(success());
                    dispatch(loadDokkuStatus());
                },
                error => {
                    dispatch(failure(error.toString()));
                    dispatch(loadDokkuStatus());
                }
            );
    };

    function request(appName: string, domain: string) {
        return {type: DokkuConstants.ADD_DOMAIN_REQUEST, appName, domain};
    }

    function success() {
        return {type: DokkuConstants.ADD_DOMAIN_SUCCESS};
    }

    function failure(error) {
        return {type: DokkuConstants.ADD_DOMAIN_FAILURE, payload: error};
    }
}

function deleteDomain(appName: string, domain: string) {
    return dispatch => {
        dispatch(request(appName, domain));
        DokkuService.deleteDomain(appName, domain)
            .then(
                () => {
                    dispatch(success());
                    dispatch(loadDokkuStatus());
                },
                error => {
                    dispatch(failure(error.toString()));
                    dispatch(loadDokkuStatus());
                }
            );
    };

    function request(appName: string, domain: string) {
        return {type: DokkuConstants.DELETE_DOMAIN_REQUEST, payload: domain};
    }

    function success() {
        return {type: DokkuConstants.DELETE_DOMAIN_SUCCESS};
    }

    function failure(error) {
        return {type: DokkuConstants.DELETE_DOMAIN_FAILURE, payload: error};
    }
}

function addPortMapping(appName: string, portMapping: string) {
    return dispatch => {
        dispatch(request(appName, portMapping));
        DokkuService.addPortMapping(appName, portMapping)
            .then(
                () => {
                    dispatch(success());
                    dispatch(loadDokkuStatus());
                },
                error => {
                    dispatch(failure(error.toString()));
                    dispatch(loadDokkuStatus());
                }
            );
    };

    function request(appName: string, portMapping: string) {
        return {type: DokkuConstants.ADD_PORT_REQUEST, appName, port: portMapping};
    }

    function success() {
        return {type: DokkuConstants.ADD_PORT_SUCCESS};
    }

    function failure(error) {
        return {type: DokkuConstants.ADD_PORT_FAILURE, payload: error};
    }
}

function deletePortMapping(appName: string, portMapping: string) {
    return dispatch => {
        dispatch(request(appName, portMapping));
        DokkuService.deletePortMapping(appName, portMapping)
            .then(
                () => {
                    dispatch(success());
                    dispatch(loadDokkuStatus());
                },
                error => {
                    dispatch(failure(error.toString()));
                    dispatch(loadDokkuStatus());
                }
            );
    };

    function request(appName: string, portMapping: string) {
        return {type: DokkuConstants.DELETE_PORT_REQUEST, payload: portMapping};
    }

    function success() {
        return {type: DokkuConstants.DELETE_PORT_SUCCESS};
    }

    function failure(error) {
        return {type: DokkuConstants.DELETE_PORT_FAILURE, payload: error};
    }
}

function addStorageMount(appName: string, storageMount: string) {
    return dispatch => {
        dispatch(request(appName, storageMount));
        DokkuService.addStorageMount(appName, storageMount)
            .then(
                () => {
                    dispatch(success());
                    dispatch(loadDokkuStatus());
                },
                error => {
                    dispatch(failure(error.toString()));
                    dispatch(loadDokkuStatus());
                }
            );
    };

    function request(appName: string, storageMount: string) {
        return {type: DokkuConstants.ADD_STORAGE_REQUEST, appName, storageMount};
    }

    function success() {
        return {type: DokkuConstants.ADD_STORAGE_SUCCESS};
    }

    function failure(error) {
        return {type: DokkuConstants.ADD_STORAGE_FAILURE, payload: error};
    }
}

function deleteStorageMount(appName: string, storageMount: string) {
    return dispatch => {
        dispatch(request(appName, storageMount));
        DokkuService.deleteStorageMount(appName, storageMount)
            .then(
                () => {
                    dispatch(success());
                    dispatch(loadDokkuStatus());
                },
                error => {
                    dispatch(failure(error.toString()));
                    dispatch(loadDokkuStatus());
                }
            );
    };

    function request(appName: string, storageMount: string) {
        return {type: DokkuConstants.DELETE_STORAGE_REQUEST, payload: storageMount};
    }

    function success() {
        return {type: DokkuConstants.DELETE_STORAGE_SUCCESS};
    }

    function failure(error) {
        return {type: DokkuConstants.DELETE_STORAGE_FAILURE, payload: error};
    }
}
