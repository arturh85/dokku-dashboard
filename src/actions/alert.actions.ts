import {AlertConstants} from "../constants/alert.constants";

export class AlertActions {
    static success = success;
    static error = error;
    static clear = clear;
}

function success(message) {
    return {type: AlertConstants.SUCCESS, message};
}

function error(message) {
    return {type: AlertConstants.ERROR, message};
}

function clear() {
    return {type: AlertConstants.CLEAR};
}