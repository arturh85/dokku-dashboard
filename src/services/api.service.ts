import * as request from 'request';
import {ErrorResponse} from "../models/api.model";
import {authHeader} from "../helpers";

export function callBackendApi<T>(apiPath: string, options: Object): Promise<T> {
    return new Promise<T>((resolve: (value: T) => void, reject: (value: Error | ErrorResponse) => void) => {
        const base = location.protocol + '//' + location.host;
        const headers = {...options['headers'] || {}, ...authHeader()};
        request(base + apiPath, {
            ...options,
            headers: headers,
        }, (err, response, body) => {
            if (err) {
                reject(err);
            } else if (response.statusCode !== 200) {
                reject(body);
            } else {
                resolve(<T>body);
            }
        })
    });
}
