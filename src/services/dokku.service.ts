import {callBackendApi} from './api.service';
import {AppStatusReport} from "../models/dokku.model.";

export class DokkuService {
    static loadDokkuStatus(): Promise<void | AppStatusReport> {
        return callBackendApi<AppStatusReport>('/api/dokku-status', {
            method: 'POST',
            json: true
        })
    }

    static updateStatus(appName: string): Promise<void> {
        return callBackendApi<void>('/api/update-status', {
            json: true,
            method: 'POST',
            body: {
                appName: appName
            }
        })
    }

    static loadLogs(appName: string, logLines: number): Promise<string> {
        return callBackendApi<string>('/api/load-logs', {
            json: true,
            method: 'POST',
            body: {
                appName: appName,
                logLines: logLines
            }
        })
    }

    static updateStatusReport(): Promise<void> {
        return callBackendApi<void>('/api/update-status-report', {
            json: true,
            method: 'POST',
        })
    }


    static letsencrypt(appName: string): Promise<void> {
        return callBackendApi<void>('/api/letsencrypt', {
            json: true,
            method: 'POST',
            body: {
                appName: appName
            }
        })
    }

    static rebuildApp(appName: string): Promise<void> {
        return callBackendApi<void>('/api/dokku-rebuild-app', {
            json: true,
            method: 'POST',
            body: {
                appName: appName
            }
        })
    }

    static addApp(appName: string, tagName: string, templateName: string): Promise<void> {
        return callBackendApi<void>('/api/dokku-add-app', {
            json: true,
            method: 'POST',
            body: {
                appName, tagName, templateName
            }
        })
    }

    static deleteApp(appName: string): Promise<void> {
        return callBackendApi<void>('/api/dokku-delete-app', {
            json: true,
            method: 'POST',
            body: {
                appName: appName
            }
        })
    }

    static unlockApp(appName: string): Promise<void> {
        return callBackendApi<void>('/api/dokku-unlock-app', {
            json: true,
            method: 'POST',
            body: {
                appName: appName
            }
        })
    }

    static addDomain(appName: string, domain: string): Promise<void> {
        return callBackendApi<void>('/api/dokku-add-domain', {
            json: true,
            method: 'POST',
            body: {
                appName: appName,
                domain: domain
            }
        })
    }

    static deleteDomain(appName: string, domain: string): Promise<void> {
        return callBackendApi<void>('/api/dokku-delete-domain', {
            json: true,
            method: 'POST',
            body: {
                appName: appName,
                domain: domain
            }
        })
    }

    static addPortMapping(appName: string, portMapping: string): Promise<void> {
        return callBackendApi<void>('/api/dokku-add-port', {
            json: true,
            method: 'POST',
            body: {
                appName: appName,
                portMapping: portMapping
            }
        })
    }

    static deletePortMapping(appName: string, portMapping: string): Promise<void> {
        return callBackendApi<void>('/api/dokku-delete-port', {
            json: true,
            method: 'POST',
            body: {
                appName: appName,
                portMapping: portMapping
            }
        })
    }

    static addStorageMount(appName: string, storageMount: string): Promise<void> {
        return callBackendApi<void>('/api/dokku-add-storage', {
            json: true,
            method: 'POST',
            body: {
                appName: appName,
                storageMount: storageMount
            }
        })
    }

    static deleteStorageMount(appName: string, storageMount: string): Promise<void> {
        return callBackendApi<void>('/api/dokku-delete-storage', {
            json: true,
            method: 'POST',
            body: {
                appName: appName,
                storageMount: storageMount
            }
        })
    }
}
