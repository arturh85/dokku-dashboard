import {authHeader} from '../helpers';

export class UserService {
    static login(username, password) {
        const requestOptions = {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify({username, password})
        };

        return fetch(`/users/authenticate`, requestOptions)
            .then(this.handleResponse)
            .then(user => {
                // login successful if there's a jwt token in the response
                if (user.token) {
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('user', JSON.stringify(user));
                }

                return user;
            });
    }

    static logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('user');
    }

    //
    // static getAll() {
    //     const requestOptions = {
    //         method: 'GET',
    //         headers: authHeader()
    //     };
    //
    //     return fetch(`/users`, requestOptions).then(this.handleResponse);
    // }
    //
    // static getById(id) {
    //     const requestOptions = {
    //         method: 'GET',
    //         headers: authHeader()
    //     };
    //
    //     return fetch(`/users/${id}`, requestOptions).then(this.handleResponse);
    // }

    static register(user) {
        const requestOptions = {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify(user)
        };

        return fetch(`/users/register`, requestOptions).then(this.handleResponse);
    }

    static update(user) {
        const requestOptions = {
            method: 'PUT',
            headers: {...authHeader(), 'Content-Type': 'application/json'},
            body: JSON.stringify(user)
        };

        return fetch(`/users/${user.id}`, requestOptions).then(this.handleResponse);
    }

    // prefixed name with underscore because delete is a reserved word in javascript
    // _delete(id) {
    //     const requestOptions = {
    //         method: 'DELETE',
    //         headers: authHeader()
    //     };
    //
    //     return fetch(`/users/${id}`, requestOptions).then(this.handleResponse);
    // }

    static handleResponse = (response) => {
        return response.text().then(text => {
            const data = text && JSON.parse(text);
            if (!response.ok) {
                if (response.status === 401) {
                    // auto logout if 401 response returned from api
                    UserService.logout();
                    location.reload(true);
                }

                const error = (data && data.message) || response.statusText;
                return Promise.reject(error);
            }

            return data;
        });
    }
}