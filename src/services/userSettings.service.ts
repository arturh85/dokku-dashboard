import {User} from "../models/user.model";

const SETTINGS_KEY = 'userSettings';

export type UserSettings = {
    currentBranch: string;
    currentContentPath: string;
};

const initialUserSettings = {
    currentBranch: null,
    currentContentPath: null,
}

const settings: UserSettings = {...initialUserSettings, ...JSON.parse(localStorage.getItem(SETTINGS_KEY)) || {}};

export class UserSettingsService {
    static getCurrentBranch(): string {
        if (settings.currentBranch) {
            return settings.currentBranch;
        } else {
            let user: User = JSON.parse(localStorage.getItem('user'));
            if (user) {
                settings.currentBranch = user.username;
                return user.username;
            }
        }
    }

    static setCurrentBranch(branchName: string) {
        settings.currentBranch = branchName;
        UserSettingsService.saveSettings();
    }

    static getCurrentContentPath(): string {
        if (settings.currentContentPath) {
            return settings.currentContentPath;
        } else {
            return '/';
        }
    }

    static setCurrentContentPath(contentPath: string) {
        settings.currentContentPath = contentPath;
        UserSettingsService.saveSettings();
    }

    static saveSettings() {
        localStorage.setItem(SETTINGS_KEY, JSON.stringify(settings));
    }
}