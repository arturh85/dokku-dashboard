import {callBackendApi} from './api.service';
import {AppStatusReport} from "../models/dokku.model.";

export class GitlabService {
    static loadTemplates(): Promise<void | AppStatusReport> {
        return callBackendApi<AppStatusReport>('/api/list-dokku-templates', {
            method: 'POST',
            json: true
        })
    }
}
