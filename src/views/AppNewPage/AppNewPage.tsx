import * as React from 'react';
import {Checkbox, Col, ControlLabel, FormControl, FormGroup, Grid, Row} from "react-bootstrap";
import {connect} from 'react-redux';
import {DokkuState} from '../../reducers/dokku.reducer';
import {AuthenticationState} from '../../reducers/authentication.reducer';
import {DokkuActions} from '../../actions/dokku.actions';
import Button from "../../components/CustomButton/CustomButton";
import Radio from "../../components/CustomRadio/CustomRadio";
import {loadingAnimation} from "../OverviewPage/OverviewPage";
const Linkify = require('linkifyjs/react');

type AppNewPageState = {
    newAppName: string;
    newTagName: string;
    newAppTemplate: string;
    newAppPort: string;
};
type AppNewPageProps = DokkuState & AuthenticationState & { dispatch: any, location: any };

class AppNewPage extends React.Component<AppNewPageProps, AppNewPageState> {
    state: AppNewPageState = {
        newAppName: '',
        newTagName: '',
        newAppTemplate: '',
        newAppPort: '',
    };

    static defaultProps = {};

    addApp(newAppName: string, newTagName: string, newAppPort: string, newAppTemplate, e) {
        e.preventDefault();
        this.props.dispatch(DokkuActions.addApp(newAppName.trim().toLowerCase(), newTagName.trim().toLowerCase(), newAppPort.trim(), newAppTemplate.trim()));
        this.setState({...this.state, newAppName: '', newAppPort: '', newTagName: '', newAppTemplate: ''});
    }

    updateNewAppName(e) {
        this.setState({...this.state, newAppName: e.target.value});
    }

    updateNewTagName(e) {
        this.setState({...this.state, newTagName: e.target.value});
    }

    updateNewAppTemplate(e) {
        this.setState({...this.state, newAppTemplate: e.target.value});
    }

    updateNewAppPort(e) {
        this.setState({...this.state, newAppPort: e.target.value});
    }

    componentDidMount() {
        this.props.dispatch(DokkuActions.loadDokkuStatus());
        this.props.dispatch(DokkuActions.loadTemplates());
    }

    render() {
        const {addAppLoading, dokkuTemplates} = this.props;
        return (
            <div className="content">
                <Grid fluid>
                    <Row key={-4}>
                        <Col lg={12} sm={12}>
                            <h4>Add new App</h4>
                            <form
                                onSubmit={(e) => this.addApp(this.state.newAppName, this.state.newTagName, this.state.newAppPort, this.state.newAppTemplate, e)}>
                                <FormGroup controlId="addDomain">
                                    <ControlLabel>App Name (a-z, 0-9, "_") for Subdomain</ControlLabel>
                                    <FormControl
                                        required
                                        pattern="[_A-Za-z0-9]+"
                                        value={this.state.newAppName}
                                        onChange={e => this.updateNewAppName(e)}
                                        placeholder="myawesomeapp"
                                    />
                                </FormGroup>
                                <FormGroup controlId="addTag">
                                    <ControlLabel>Project Tag (a-z, 0-9, "-") for Gitlab</ControlLabel>
                                    <FormControl
                                        required
                                        pattern="[-A-Za-z0-9]+"
                                        value={this.state.newTagName}
                                        onChange={e => this.updateNewTagName(e)}
                                        placeholder="my-awesome-app"
                                    />
                                </FormGroup>

                                <FormGroup controlId="selectTemplate">
                                    <ControlLabel>Deploy with{this.state.newAppTemplate ? (' "' + this.state.newAppTemplate + '"') : ':'}</ControlLabel>
                                    <Radio name="selectTemplate" value="" checked={this.state.newAppTemplate === ''}
                                           key={-1} onChange={e => this.updateNewAppTemplate(e)}>
                                        <p>Leave undeployed</p>
                                    </Radio>
                                    {dokkuTemplates.map((dokkuTemplate, index) => {
                                        return (
                                            <Radio checked={this.state.newAppTemplate === dokkuTemplate.name}
                                                   value={dokkuTemplate.name}
                                                   name="selectTemplate"
                                                   key={index}
                                                   onChange={e => this.updateNewAppTemplate(e)}
                                            >
                                                <Linkify tagName="p" options={{}}>{ dokkuTemplate.description}</Linkify>
                                                <img src={dokkuTemplate.avatar_url}
                                                     style={{width:"150px"}}
                                                     title={dokkuTemplate.name}/>
                                                <p>&nbsp;</p>
                                            </Radio>
                                        )
                                    })}
                                </FormGroup>

                                {/*{this.state.newAppTemplate === '' && <FormGroup controlId="ports">*/}
                                  {/*<ControlLabel>Setup HTTP Port for Proxy</ControlLabel>*/}
                                  {/*<FormControl*/}
                                    {/*value={this.state.newAppPort}*/}
                                    {/*onChange={e => this.updateNewAppPort(e)}*/}
                                    {/*placeholder="5000"*/}
                                  {/*/>*/}
                                {/*</FormGroup>}*/}

                                <Button bsStyle="primary" pullRight fill type="submit">
                                    Add {addAppLoading && loadingAnimation}
                                </Button><br/><br/><br/>
                            </form>
                        </Col>
                    </Row>
                </Grid>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {...state.dokku, ...state.authentication};
}

const connected = connect(mapStateToProps)(AppNewPage);
export {connected as AppNewPage};
