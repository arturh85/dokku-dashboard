import * as React from 'react';
import {Col, ControlLabel, FormControl, FormGroup, Grid, Row, Modal, DropdownButton, MenuItem} from "react-bootstrap";
import {connect} from 'react-redux';
import {AppStatus, ProxyPortsEntry} from "../../models/dokku.model.";
import {DokkuState} from '../../reducers/dokku.reducer';
import {AuthenticationState} from '../../reducers/authentication.reducer';
import {DokkuActions} from '../../actions/dokku.actions';
import Button from "../../components/CustomButton/CustomButton";
import * as cloneDeep from 'lodash/cloneDeep';
import {loadingAnimation} from "../OverviewPage/OverviewPage";
var Convert = require('ansi-to-html');
var convert = new Convert();
var linkifyHtml = require('linkifyjs/html');
type DokkuPageState = {
    newDomainByAppname: { [appName: string]: string };
    newPortByAppname: { [appName: string]: string };
    newStorageByAppname: { [appName: string]: string };
    showLogs: boolean
    showLogLines: number
};
type DokkuPageProps = DokkuState & AuthenticationState & { dispatch: any, location: any, match: any };

const defaultShowLogLines = 100;

class AppDetailPage extends React.Component<DokkuPageProps, DokkuPageState> {
    state: DokkuPageState = {
        newDomainByAppname: {},
        newPortByAppname: {},
        newStorageByAppname: {},
        showLogs: false,
        showLogLines: defaultShowLogLines
    };

    static defaultProps = {};

    unlockApp(newAppName: string) {
        this.props.dispatch(DokkuActions.unlockApp(newAppName));
    }

    updateStatus(appName: string) {
        this.props.dispatch(DokkuActions.updateStatus(appName));
    }

    letsencrypt(appName: string) {
        this.props.dispatch(DokkuActions.letsencrypt(appName));
    }

    deleteApp(appName: string) {
        this.props.dispatch(DokkuActions.deleteApp(appName));
    }

    rebuildApp(appName: string) {
        this.props.dispatch(DokkuActions.rebuildApp(appName));
    }

    addDomain(appName: string, domain: string, e) {
        e.preventDefault();
        this.props.dispatch(DokkuActions.addDomain(appName, domain));
        let newDomainByAppname = cloneDeep(this.state.newDomainByAppname);
        newDomainByAppname[appName] = '';
        this.setState({...this.state, newDomainByAppname: newDomainByAppname});
    }

    deleteDomain(appName: string, domain: string) {
        this.props.dispatch(DokkuActions.deleteDomain(appName, domain));
    }

    updateDomainInput(appName, e) {
        let newDomainByAppname = cloneDeep(this.state.newDomainByAppname);
        newDomainByAppname[appName] = e.target.value;
        this.setState({...this.state, newDomainByAppname: newDomainByAppname});
    }

    addPort(appName: string, portMapping: string, e) {
        e.preventDefault();
        this.props.dispatch(DokkuActions.addPort(appName, portMapping));
        let newPortByAppname = cloneDeep(this.state.newPortByAppname);
        newPortByAppname[appName] = '';
        this.setState({...this.state, newPortByAppname: newPortByAppname});
    }

    updatePortInput(appName, e) {
        let newPortByAppname = cloneDeep(this.state.newPortByAppname);
        newPortByAppname[appName] = e.target.value;
        this.setState({...this.state, newPortByAppname: newPortByAppname});
    }

    logs(appName) {
        this.props.dispatch(DokkuActions.loadLogs(appName, this.state.showLogLines));
        this.setState({...this.state, showLogs: true});
    }

    updateLogLines = (lines) => {
        this.setState({...this.state, showLogLines: lines});
        this.props.dispatch(DokkuActions.loadLogs(this.props.loadLogsAppName, lines));
    };

    closeLogs = () => {
        this.setState({...this.state, showLogs: false, showLogLines: defaultShowLogLines});
    };

    deleteProxyPort(appName: string, proxyPort: ProxyPortsEntry) {
        const portMapping = proxyPort.scheme + ':' + proxyPort.hostPort + ':' + proxyPort.containerPort;
        this.props.dispatch(DokkuActions.deletePort(appName, portMapping));
    }

    addStorageMount(appName: string, storageMount: string, e) {
        e.preventDefault();
        this.props.dispatch(DokkuActions.addStorageMount(appName, storageMount));
        let newStorageByAppname = cloneDeep(this.state.newStorageByAppname);
        newStorageByAppname[appName] = '';
        this.setState({...this.state, newStorageByAppname: newStorageByAppname});
    }

    updateStorageMountInput(appName, e) {
        let newStorageByAppname = cloneDeep(this.state.newStorageByAppname);
        newStorageByAppname[appName] = e.target.value;
        this.setState({...this.state, newStorageByAppname: newStorageByAppname});
    }

    deleteStorageMount(appName: string, storageMount: string) {
        this.props.dispatch(DokkuActions.deleteStorageMount(appName, storageMount));
    }

    componentDidMount() {
        this.props.dispatch(DokkuActions.loadDokkuStatus());
    }

    escapeHtml = (unsafe) => {
        if (!unsafe) {
            return '';
        }
        return unsafe
            .replace(/&/g, "&amp;")
            .replace(/</g, "&lt;")
            .replace(/>/g, "&gt;")
            .replace(/"/g, "&quot;")
            .replace(/'/g, "&#039;");
    };

    linkify = (html) => {
        return linkifyHtml(html, {
            defaultProtocol: 'https',
            validate: {
                url: function (value) {
                    return /^(http|ftp)s?:\/\//.test(value);
                }
            }
        });
    };

    render() {
        const {
            dokkuStatus,
            loadDokkuLoading,
            loadDokkuError,
            updateStatusLoading,
            letsencryptLoading,
            unlockLoading,
            deleteAppLoading,
            deleteDomainLoading,
            deletePortLoading,
            addDomainLoading,
            addPortLoading,
            rebuildAppLoading,
            addStorageLoading,
            deleteStorageLoading,
            loadLogsAppName,
            loadLogsLoading,
            loadLogsError,
            appLogs
        } = this.props;
        const appName = this.props.match.params.appName;
        Object.keys(dokkuStatus).forEach(iterAppName => {
            if (!this.state.newDomainByAppname[iterAppName]) {
                this.state.newDomainByAppname[iterAppName] = '';
            }
            if (!this.state.newPortByAppname[iterAppName]) {
                this.state.newPortByAppname[iterAppName] = '';
            }
            if (!this.state.newStorageByAppname[iterAppName]) {
                this.state.newStorageByAppname[iterAppName] = '';
            }
        });
        let gitlabProjectName = appName;
        if (dokkuStatus && dokkuStatus[appName] && dokkuStatus[appName]['config']['GITLAB_PROJECTNAME']) {
            gitlabProjectName = dokkuStatus[appName]['config']['GITLAB_PROJECTNAME']
        }
        if (appName === 'www') {
            gitlabProjectName = 'www.app42.blue';
        }
        return (
            <div className="content">
                <Modal show={this.state.showLogs} onHide={this.closeLogs} bsSize="large" dialogClassName="custom-modal">
                    <Modal.Header closeButton>
                        <Modal.Title>
                            Logs for {loadLogsAppName}

                            <DropdownButton title={this.state.showLogLines + " log lines"}
                                            id="bg-vertical-dropdown-1"
                                            style={{margin: "5px"}}
                                            onSelect={logLines => this.updateLogLines(logLines)}>
                                <MenuItem eventKey={100}>100</MenuItem>
                                <MenuItem eventKey={500}>500</MenuItem>
                                <MenuItem eventKey={1000}>1000</MenuItem>
                                <MenuItem eventKey={5000}>5000</MenuItem>
                            </DropdownButton>

                            {!loadLogsLoading &&
                            <span style={{margin: "5px"}}><Button bsStyle="primary" fill
                                    onClick={() => this.logs(loadLogsAppName)}>
                              Refresh
                            </Button></span>}
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        {loadLogsLoading && loadingAnimation}
                        {loadLogsError && <span>error!</span>}
                        <pre dangerouslySetInnerHTML={{__html: this.linkify(convert.toHtml(this.escapeHtml(appLogs)))}}>

                        </pre>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button onClick={this.closeLogs}>Close</Button>
                    </Modal.Footer>
                </Modal>

                <Grid fluid>
                    {loadDokkuLoading && <Row key={-1}>
                      <Col lg={12} sm={12}>
                        <h4>Loading {loadingAnimation}</h4>
                      </Col>
                    </Row>}
                    {loadDokkuError && <Row key={-2}>
                      <Col lg={12} sm={12}>
                        <h4>Failed to load, see server logs</h4>
                      </Col>
                    </Row>}

                    <Row key={-3}>
                        <Col lg={12} sm={12}>
                            {Object.keys(dokkuStatus).filter(name => name === appName).map(appName => {
                                const appStatus: AppStatus = dokkuStatus[appName];
                                return (
                                    <div className="card" key={appName}>
                                        <div className="header"
                                             style={{backgroundColor: !appStatus.appReport ? 'lightgreen' : (appStatus.appReport.Locked === 'true' ? 'rgb(255, 135, 142)' : 'lightblue')}}>
                                            <h4 className="title">{appName}: {appStatus.appReport ? appStatus.appReport["Git sha"] : 'not deployed'} &nbsp;&nbsp;
                                                <Button bsStyle="primary" fill
                                                        onClick={() => this.updateStatus(appName)}>
                                                    Refresh {updateStatusLoading && loadingAnimation}
                                                </Button>


                                                <DropdownButton title="Gitlab"
                                                                bsStyle="primary" fill
                                                                style={{margin: "5px"}}>
                                                    <MenuItem href={'https://gitlab.app42.blue/dokku/' + gitlabProjectName + '/-/jobs'}>Jobs</MenuItem>
                                                </DropdownButton>

                                                {<a href={'https://gitlab.app42.blue/dokku/' + gitlabProjectName + '/-/jobs'}><Button
                                                    bsStyle="primary" fill>
                                                    Jobs
                                                </Button></a>}
                                                {appStatus.appReport &&
                                                <Button bsStyle="primary" fill onClick={() => this.rebuildApp(appName)}>
                                                  Rebuild {rebuildAppLoading && loadingAnimation}
                                                </Button>}
                                                {appStatus.appReport &&
                                                <Button bsStyle="primary" fill
                                                        onClick={() => this.letsencrypt(appName)}>
                                                  Let’s Encrypt {letsencryptLoading && loadingAnimation}
                                                    {appStatus.letsEncrypt && <span>✅</span>}
                                                </Button>}
                                                {appStatus.appReport &&
                                                <Button bsStyle="primary" fill
                                                        onClick={() => this.logs(appName)}>
                                                  Logs
                                                </Button>}
                                                <Button bsStyle="danger" pullRight fill
                                                        onClick={() => this.deleteApp(appName)}>
                                                    Destroy {deleteAppLoading && loadingAnimation}
                                                </Button>
                                                {appStatus.appReport && appStatus.appReport.Locked === 'true' &&
                                                <Button bsStyle="danger" pullRight fill
                                                        onClick={() => this.unlockApp(appName)}>
                                                  Unlock {unlockLoading && loadingAnimation}
                                                </Button>
                                                }
                                            </h4>

                                            <p className="category"/>
                                        </div>
                                        <div className="content">
                                            <Grid fluid>
                                                <Row>
                                                    {appStatus.domains && appStatus.domains["Domains app vhosts"]
                                                    && <Col lg={4} md={4} sm={12}>
                                                      <h4>Domains</h4>
                                                        {appStatus.domains["Domains app vhosts"].map(domain => (
                                                            <div key={domain} className="alert alert-entry">
                                                                <a href={(appStatus.proxyPorts.find(portMapping => portMapping.scheme === 'https') ? 'https://' : 'http://') + domain}>{domain}</a>&nbsp;
                                                                {deleteDomainLoading && <span
                                                                  style={{float: 'right'}}>{loadingAnimation}</span>}
                                                                {!deleteDomainLoading && <button
                                                                  onClick={() => this.deleteDomain(appName, domain)}
                                                                  type="button" aria-hidden="true"
                                                                  className="close">✕
                                                                </button>}
                                                            </div>
                                                        ))}

                                                      <h4>Add new Domain</h4>
                                                      <form
                                                        onSubmit={e => this.addDomain(appName, this.state.newDomainByAppname[appName], e)}>
                                                        <FormGroup controlId="addDomain">
                                                          <ControlLabel>Domain Name</ControlLabel>
                                                          <FormControl
                                                            value={this.state.newDomainByAppname[appName]}
                                                            onChange={e => this.updateDomainInput(appName, e)}
                                                            placeholder="www.example.com"
                                                          />
                                                        </FormGroup>
                                                        <Button bsStyle="primary" pullRight fill type="submit">
                                                          Add {addDomainLoading && loadingAnimation}
                                                        </Button>
                                                      </form>

                                                    </Col>}

                                                    {appStatus.proxyPorts && <Col lg={4} md={4} sm={12}>
                                                      <h4>Ports</h4>
                                                        {appStatus.proxyPorts.map((proxyPort, index) => (
                                                            <div key={index} className="alert alert-entry">
                                                                {proxyPort.scheme}:{proxyPort.hostPort}:{proxyPort.containerPort}&nbsp;
                                                                {deletePortLoading && <span
                                                                  style={{float: 'right'}}>{loadingAnimation}</span>}
                                                                {!deletePortLoading && <button
                                                                  onClick={() => this.deleteProxyPort(appName, proxyPort)}
                                                                  type="button" aria-hidden="true"
                                                                  className="close">✕
                                                                </button>}
                                                            </div>
                                                        ))}
                                                      <h4>Add new Port Mapping</h4>
                                                      <form
                                                        onSubmit={e => this.addPort(appName, this.state.newPortByAppname[appName], e)}>
                                                        <FormGroup controlId="addPort">
                                                          <ControlLabel>(Schema):(Host Port):(Container
                                                            Port)</ControlLabel>
                                                          <FormControl
                                                            value={this.state.newPortByAppname[appName]}
                                                            onChange={e => this.updatePortInput(appName, e)}
                                                            placeholder="http:443:5000"
                                                          />
                                                        </FormGroup>
                                                        <Button bsStyle="primary" pullRight fill type="submit">
                                                          Add {addPortLoading && loadingAnimation}
                                                        </Button>
                                                      </form>
                                                    </Col>}

                                                    {appStatus.storage && <Col lg={4} md={4} sm={12}>
                                                      <h4>Mounts</h4>
                                                        {appStatus.storage.map((storageMount, index) => (
                                                            <div key={index} className="alert alert-entry">
                                                                {storageMount}&nbsp;
                                                                {deleteStorageLoading && <span
                                                                  style={{float: 'right'}}>{loadingAnimation}</span>}
                                                                {!deleteStorageLoading && <button
                                                                  onClick={() => this.deleteStorageMount(appName, storageMount)}
                                                                  type="button" aria-hidden="true"
                                                                  className="close">✕
                                                                </button>}
                                                            </div>
                                                        ))}
                                                      <h4>Add new Mount</h4>
                                                      <form
                                                        onSubmit={e => this.addStorageMount(appName, this.state.newStorageByAppname[appName], e)}>
                                                        <FormGroup controlId="addStorageMount">
                                                          <ControlLabel>(Host Path):(Container Path)</ControlLabel>
                                                          <FormControl
                                                            value={this.state.newStorageByAppname[appName]}
                                                            onChange={e => this.updateStorageMountInput(appName, e)}
                                                            placeholder="/hostPath:/containerPath"
                                                          />
                                                        </FormGroup>
                                                        <Button bsStyle="primary" pullRight fill type="submit">
                                                          Add {addStorageLoading && loadingAnimation}
                                                        </Button>
                                                      </form>
                                                    </Col>}

                                                </Row>
                                            </Grid>

                                            <div className="footer">
                                                <div className="stats"><i/></div>
                                            </div>
                                        </div>
                                    </div>
                                )
                            })}
                        </Col>
                    </Row>
                </Grid>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {...state.dokku, ...state.authentication};
}

const connected = connect(mapStateToProps)(AppDetailPage);
export {connected as AppDetailPage};
