import * as React from 'react';
import {Col, ControlLabel, FormControl, FormGroup, Grid, Row} from "react-bootstrap";
import {connect} from 'react-redux';
import {AppStatus} from "../../models/dokku.model.";
import {DokkuState} from '../../reducers/dokku.reducer';
import {AuthenticationState} from '../../reducers/authentication.reducer';
import {DokkuActions} from '../../actions/dokku.actions';
import Button from "../../components/CustomButton/CustomButton";
import {history} from '../../helpers';

type DokkuPageState = {
    newDomainByAppname: { [appName: string]: string };
    newPortByAppname: { [appName: string]: string };
    newAppName: string;
};
type DokkuPageProps = DokkuState & AuthenticationState & { dispatch: any };

export const loadingAnimation = <img
    src="data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA=="/>;

class OverviewPage extends React.Component<DokkuPageProps, DokkuPageState> {
    state: DokkuPageState = {
        newDomainByAppname: {},
        newPortByAppname: {},
        newAppName: ''
    };

    static defaultProps = {};

    updateStatusReport() {
        this.props.dispatch(DokkuActions.updateStatusReport());
    }

    componentDidMount() {
        this.props.dispatch(DokkuActions.loadDokkuStatus());
    }

    render() {
        const {dokkuStatus, loadDokkuLoading, loadDokkuError, updateStatusReportLoading} = this.props;
        Object.keys(dokkuStatus).forEach(appName => {
            if (!this.state.newDomainByAppname[appName]) {
                this.state.newDomainByAppname[appName] = '';
            }
            if (!this.state.newPortByAppname[appName]) {
                this.state.newPortByAppname[appName] = '';
            }
        });

        const sortedAppNames = Object.keys(this.props.dokkuStatus).sort((a, b) => {
            const aa = !this.props.dokkuStatus[a].appReport;
            const bb = !this.props.dokkuStatus[b].appReport;
            if (aa !== bb) {
                return aa ? -2 : 2;
            }
            return a < b ? -1 : 1;
        });
        return (
            <div className="content">
                <Grid fluid>
                    {loadDokkuLoading && <Row key={-1}>
                      <Col lg={12} sm={12}>
                        <h4>Loading</h4>
                      </Col>
                    </Row>}
                    {loadDokkuError && <Row key={-2}>
                      <Col lg={12} sm={12}>
                        <h4>Failed to load</h4>
                      </Col>
                    </Row>}

                    <Button bsStyle="warning" pullRight fill onClick={() => this.updateStatusReport()}>
                        Refresh all hard {updateStatusReportLoading && loadingAnimation}
                    </Button><br/><br/><br/>

                    <Row key={-3}>
                        <Col lg={12} sm={12}>
                            {sortedAppNames.map(appName => {
                                const appStatus: AppStatus = dokkuStatus[appName];
                                return (
                                    <div className="card" key={appName}>
                                        <div className="header"
                                             style={{backgroundColor: !appStatus.appReport ? 'lightgreen' : (appStatus.appReport.Locked === 'true' ? 'rgb(255, 135, 142)' : 'lightblue')}}>
                                            <h4 className="title">{appName}: {appStatus.appReport ? appStatus.appReport["Git sha"] : 'not deployed'}
                                                &nbsp;&nbsp;
                                                <Button bsStyle="primary" fill
                                                        onClick={() => history.push('/app/' + appName)}>
                                                    Details
                                                </Button>
                                            </h4>
                                            <p className="category"/>
                                        </div>
                                        <div className="content">
                                            {appStatus.appReport && appStatus.appReport.Locked === 'true' && <div>
                                              Locked!
                                            </div>}
                                            <Grid fluid>
                                                <Row>
                                                    {appStatus.domains && appStatus.domains["Domains app vhosts"]
                                                    && appStatus.domains["Domains app vhosts"].length > 0
                                                    && <Col lg={4} md={4} sm={12}>
                                                      <h4>Domains</h4>
                                                        {appStatus.domains["Domains app vhosts"].map(domain => (
                                                            <div key={domain} className="alert alert-entry">
                                                                <a href={(appStatus.proxyPorts.find(portMapping => portMapping.scheme === 'https') ? 'https://' : 'http://') + domain}>{domain}</a>
                                                            </div>
                                                        ))}
                                                    </Col>}
                                                    {appStatus.proxyPorts && appStatus.proxyPorts.length > 0 &&
                                                    <Col lg={4} md={4} sm={12}>
                                                      <h4>Ports</h4>
                                                        {appStatus.proxyPorts.map((proxyPort, index) => (
                                                            <div key={index} className="alert alert-entry">
                                                                {proxyPort.scheme}:{proxyPort.hostPort}:{proxyPort.containerPort}
                                                            </div>
                                                        ))}
                                                    </Col>}
                                                    {appStatus.storage && appStatus.storage.length > 0 &&
                                                    <Col lg={4} md={4} sm={12}>
                                                      <h4>Mounts</h4>
                                                        {appStatus.storage.map((storageMount, index) => (
                                                            <div key={index} className="alert alert-entry">
                                                                {storageMount}
                                                            </div>
                                                        ))}
                                                    </Col>}
                                                </Row>
                                            </Grid>

                                            <div className="footer">
                                                <div className="stats"><i/></div>
                                            </div>
                                        </div>
                                    </div>
                                )
                            })}
                        </Col>
                    </Row>
                </Grid>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {...state.dokku, ...state.authentication};
}

const connected = connect(mapStateToProps)(OverviewPage);
export {connected as OverviewPage};