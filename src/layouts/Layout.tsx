import * as React from "react";
import {Component} from "react";
import {Redirect, Route, Switch} from "react-router-dom";
import * as NotificationSystem from "react-notification-system";
import {connect} from 'react-redux';
import Header from "components/Header/Header";
import Footer from "components/Footer/Footer";
import {Sidebar} from "components/Sidebar/Sidebar";
import * as style from "variables/Variables";
import applicationRoutes from "routes/routes";
import {AuthenticationState} from "../reducers/authentication.reducer";

type LayoutState = {
    _notificationSystem: any | null
};
type LayoutProps = AuthenticationState & { dispatch: any };

class Layout extends Component<LayoutProps, LayoutState> {
    constructor(props) {
        super(props);
        this.state = {
            _notificationSystem: null
        };
    }

    addNotification = (notification) => {
        this.state._notificationSystem.addNotification(notification);
    };

    componentDidMount() {
        // if(this.props.isLoggedIn) {
        //     this.props.dispatch(BranchActions.loadBranches());
        // }
        this.setState({_notificationSystem: this.refs.notificationSystem});
    }

    componentDidUpdate(e) {
        if (
            window.innerWidth < 993 &&
            e.history.location.pathname !== e.location.pathname &&
            document.documentElement.className.indexOf("nav-open") !== -1
        ) {
            document.documentElement.classList.toggle("nav-open");
        }
        if (e.history.action === "PUSH") {
            document.documentElement.scrollTop = 0;
            document.scrollingElement.scrollTop = 0;
            this.refs.mainPanel['scrollTop'] = 0;
        }
    }

    render() {
        const {isLoggedIn} = this.props;
        return (
            <div className="wrapper">
                <NotificationSystem ref="notificationSystem" style={style}/>
                {isLoggedIn && <Sidebar {...this.props} />}
                <div id="main-panel" className="main-panel" ref="mainPanel">
                    {isLoggedIn && <Header {...this.props} />}
                    <Switch>
                        {applicationRoutes.map((prop, key) => {
                            if (prop.redirect)
                                return <Redirect from={prop.path} to={prop.to} key={key}/>;
                            return (
                                <Route path={prop.path}
                                       render={routeProps => {
                                           return prop.public || isLoggedIn ? (
                                               <prop.component {...routeProps}
                                                               addNotification={this.addNotification}
                                               />
                                           ) : <Redirect to={{pathname: '/login', state: {from: routeProps.location}}}/>
                                       }}
                                       key={key}
                                />
                            );
                        })}
                    </Switch>
                    {isLoggedIn && <Footer/>}
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {...state.authentication};
}

const connected = connect(mapStateToProps)(Layout);
export {connected as Layout};
