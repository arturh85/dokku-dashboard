export type User = {
    id: number,
    username: string,
    email: string,
    hash?: string,
    token?: string,
    name: string,
    createdDate: string,
    permissions: string[]
};