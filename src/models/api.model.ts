export type ErrorResponse = {
    message: string
};

export const PERMISSION_ADD_APP = 'ADD_APP';
export const PERMISSION_READ_LOGS = 'READ_LOGS';
export const PERMISSION_RUN_COMMANDS = 'RUN_COMMANDS';
export const PERMISSION_WRITE_MOUNTS = 'WRITE_MOUNTS';

export const allPermissions = [PERMISSION_ADD_APP, PERMISSION_READ_LOGS, PERMISSION_RUN_COMMANDS, PERMISSION_WRITE_MOUNTS];