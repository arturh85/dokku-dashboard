export type ProxyPortsEntry = {
    scheme: string;
    hostPort: number;
    containerPort: number;
}

export type TagsEntry = {
    repository: string;
    tag: string;
    imageId: string;
    created: string;
    size: string;
}

export type AppReport = {
    'App dir': string;
    'Git sha': string;
    'Deploy source': string;
    'Locked': string;

}
export type AppDomains = {
    'Domains app enabled': string;
    'Domains app vhosts': string[];
    'Domains global enabled': string;
    'Domains global vhosts': string;

}
export type AppLetsEncrypt = {
    'Certificate Expiry': string;
    'Time before expiry': string;
    'Time before renewal': string;
}

export type AppConfig = {
    'DOKKU_DOCKERFILE_PORTS': string;
    'DOKKU_LETSENCRYPT_EMAIL': string;
    'DOKKU_PROXY_PORT_MAP': string;
    'DOKKU_PROXY_SSL_PORT': string;
    'GIT_REV': string;
    'DOKKU_APP_RESTORE': string;
    'DOKKU_APP_TYPE': string;
}

export type AppStatus = {
    appReport: AppReport;
    domains: AppDomains;
    config: AppConfig;
    storage: string[];
    proxyPorts: ProxyPortsEntry[];
    letsEncrypt: AppLetsEncrypt;
}

export type AppStatusReport = { [appName: string]: AppStatus };
