
export type SentryListClientKeysResponse = {
    "browserSdk": Object,
    "browserSdkVersion": string,
    "dateCreated": string,
    "dsn": {
        "cdn": string,
        "csp": string,
        "minidump": string,
        "public": string,
        "secret": string,
        "security": string,
    },
    "id": string,
    "isActive": boolean,
    "label": string,
    "name": string,
    "projectId": number,
    "public": string,
    "rateLimit": any,
    "secret": string
}

export type SentryCreateProjectResponse = {
    "avatar": {
        "avatarType": string,
        "avatarUuid": any
    },
    "color": string,
    "dateCreated": string,
    "features": string[],
    "firstEvent": any,
    "hasAccess": boolean,
    "id": string,
    "isBookmarked": boolean,
    "isInternal": boolean,
    "isMember": boolean,
    "isPublic": boolean,
    "name": string,
    "platform": any,
    "slug": string,
    "status": string
};
