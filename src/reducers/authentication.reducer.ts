import {UserConstants} from "../constants/user.constants";
import {User} from "../models/user.model";

let user: User = JSON.parse(localStorage.getItem('user'));

export type AuthenticationState = {
    isLoggedIn: boolean;
    loggingIn: boolean;
    user: User;
}

const loggedOutState = {loggingIn: false, isLoggedIn: false, user: null};
const initialState: AuthenticationState = user ? {loggingIn: false, isLoggedIn: true, user} : loggedOutState;

export function authentication(state = initialState, action): AuthenticationState {
    switch (action.type) {
        case UserConstants.LOGIN_REQUEST:
            return {
                isLoggedIn: true,
                loggingIn: true,
                user: action.user
            };
        case UserConstants.LOGIN_SUCCESS:
            return {
                isLoggedIn: true,
                loggingIn: false,
                user: action.user
            };
        case UserConstants.LOGIN_FAILURE:
            return loggedOutState;
        case UserConstants.LOGOUT:
            return loggedOutState;
        default:
            return state
    }
}