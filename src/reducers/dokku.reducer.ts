import {ErrorResponse} from '../models/api.model';
import {DokkuConstants} from '../constants/dokku.constants';
import {DokkuTemplate} from "../backend/api/gitlabBackendService";

export type DokkuState = {
    dokkuStatus: any;
    dokkuTemplates: DokkuTemplate[];
    appLogs: string;

    loadDokkuLoading: boolean;
    loadDokkuSuccess: boolean;
    loadDokkuError: Error | ErrorResponse;

    unlockLoading: boolean;
    unlockSuccess: boolean;
    unlockError: Error | ErrorResponse;

    loadTemplatesLoading: boolean;
    loadTemplatesSuccess: boolean;
    loadTemplatesError: Error | ErrorResponse;

    loadLogsAppName: string | null;
    loadLogsLoading: boolean;
    loadLogsSuccess: boolean;
    loadLogsError: Error | ErrorResponse;

    updateStatusLoading: boolean;
    updateStatusSuccess: boolean;
    updateStatusError: Error | ErrorResponse;

    updateStatusReportLoading: boolean;
    updateStatusReportSuccess: boolean;
    updateStatusReportError: Error | ErrorResponse;

    letsencryptLoading: boolean;
    letsencryptSuccess: boolean;
    letsencryptError: Error | ErrorResponse;

    rebuildAppLoading: boolean;
    rebuildAppSuccess: boolean;
    rebuildAppError: Error | ErrorResponse;

    addAppLoading: boolean;
    addAppSuccess: boolean;
    addAppError: Error | ErrorResponse;

    deleteAppLoading: boolean;
    deleteAppSuccess: boolean;
    deleteAppError: Error | ErrorResponse;

    addDomainLoading: boolean;
    addDomainSuccess: boolean;
    addDomainError: Error | ErrorResponse;

    deleteDomainLoading: boolean;
    deleteDomainSuccess: boolean;
    deleteDomainError: Error | ErrorResponse;

    addPortLoading: boolean;
    addPortSuccess: boolean;
    addPortError: Error | ErrorResponse;

    deletePortLoading: boolean;
    deletePortSuccess: boolean;
    deletePortError: Error | ErrorResponse;

    addStorageLoading: boolean;
    addStorageSuccess: boolean;
    addStorageError: Error | ErrorResponse;

    deleteStorageLoading: boolean;
    deleteStorageSuccess: boolean;
    deleteStorageError: Error | ErrorResponse;
}

const initialState: DokkuState = {
    dokkuStatus: {},
    dokkuTemplates: [],
    appLogs: '',

    loadDokkuLoading: false,
    loadDokkuSuccess: false,
    loadDokkuError: null,

    unlockLoading: false,
    unlockSuccess: false,
    unlockError: null,

    loadTemplatesLoading: false,
    loadTemplatesSuccess: false,
    loadTemplatesError: null,

    loadLogsAppName: null,
    loadLogsLoading: false,
    loadLogsSuccess: false,
    loadLogsError: null,

    addAppLoading: false,
    addAppSuccess: false,
    addAppError: null,

    rebuildAppLoading: false,
    rebuildAppSuccess: false,
    rebuildAppError: null,

    updateStatusLoading: false,
    updateStatusSuccess: false,
    updateStatusError: null,

    updateStatusReportLoading: false,
    updateStatusReportSuccess: false,
    updateStatusReportError: null,

    letsencryptLoading: false,
    letsencryptSuccess: false,
    letsencryptError: null,

    deleteAppLoading: false,
    deleteAppSuccess: false,
    deleteAppError: null,

    addDomainLoading: false,
    addDomainSuccess: false,
    addDomainError: null,

    deleteDomainLoading: false,
    deleteDomainSuccess: false,
    deleteDomainError: null,

    addPortLoading: false,
    addPortSuccess: false,
    addPortError: null,

    deletePortLoading: false,
    deletePortSuccess: false,
    deletePortError: null,

    addStorageLoading: false,
    addStorageSuccess: false,
    addStorageError: null,

    deleteStorageLoading: false,
    deleteStorageSuccess: false,
    deleteStorageError: null,
};

export function dokku(state: DokkuState = initialState, action): DokkuState {
    switch (action.type) {
        case DokkuConstants.LOAD_REQUEST:
            return {
                ...state,
                loadDokkuLoading: true,
                loadDokkuSuccess: false,
                loadDokkuError: null
            };
        case DokkuConstants.LOAD_SUCCESS:
            return {
                ...state,
                dokkuStatus: action.payload,
                loadDokkuLoading: false,
                loadDokkuSuccess: true
            };
        case DokkuConstants.LOAD_FAILURE:
            return {
                ...state,
                loadDokkuLoading: false,
                loadDokkuSuccess: false,
                loadDokkuError: action.payload
            };
        case DokkuConstants.LOAD_LOGS_REQUEST:
            return {
                ...state,
                appLogs: '',
                loadLogsAppName: action.payload,
                loadLogsLoading: true,
                loadLogsSuccess: false,
                loadLogsError: null
            };
        case DokkuConstants.LOAD_LOGS_SUCCESS:
            return {
                ...state,
                appLogs: action.payload,
                loadLogsLoading: false,
                loadLogsSuccess: true
            };
        case DokkuConstants.LOAD_LOGS_FAILURE:
            return {
                ...state,
                loadLogsLoading: false,
                loadLogsSuccess: false,
                loadLogsError: action.payload
            };

        case DokkuConstants.UNLOCK_REQUEST:
            return {
                ...state,
                unlockLoading: true,
                unlockSuccess: false,
                unlockError: null
            };
        case DokkuConstants.UNLOCK_SUCCESS:
            return {
                ...state,
                unlockLoading: false,
                unlockSuccess: true
            };
        case DokkuConstants.UNLOCK_FAILURE:
            return {
                ...state,
                unlockLoading: false,
                unlockSuccess: false,
                unlockError: action.payload
            };

        case DokkuConstants.LOAD_TEMPLATES_REQUEST:
            return {
                ...state,
                loadTemplatesLoading: true,
                loadTemplatesSuccess: false,
                loadTemplatesError: null
            };
        case DokkuConstants.LOAD_TEMPLATES_SUCCESS:
            return {
                ...state,
                dokkuTemplates: action.payload, /* .sort((a,b) => a.name < b.name ? -1 : 1) */
                loadTemplatesLoading: false,
                loadTemplatesSuccess: true
            };
        case DokkuConstants.LOAD_TEMPLATES_FAILURE:
            return {
                ...state,
                loadTemplatesLoading: false,
                loadTemplatesSuccess: false,
                loadTemplatesError: action.payload
            };

        case DokkuConstants.LETSENCRYPT_REQUEST:
            return {
                ...state,
                letsencryptLoading: true,
                letsencryptSuccess: false,
                letsencryptError: null
            };
        case DokkuConstants.LETSENCRYPT_SUCCESS:
            return {
                ...state,
                letsencryptLoading: false,
                letsencryptSuccess: true
            };
        case DokkuConstants.LETSENCRYPT_FAILURE:
            return {
                ...state,
                letsencryptLoading: false,
                letsencryptSuccess: false,
                letsencryptError: action.payload
            };

        case DokkuConstants.ADD_APP_REQUEST:
            return {
                ...state,
                addAppLoading: true,
                addAppSuccess: false,
                addAppError: null
            };
        case DokkuConstants.ADD_APP_SUCCESS:
            return {
                ...state,
                addAppLoading: false,
                addAppSuccess: true
            };
        case DokkuConstants.ADD_APP_FAILURE:
            return {
                ...state,
                addAppLoading: false,
                addAppSuccess: false,
                addAppError: action.payload
            };

        case DokkuConstants.REBUILD_APP_REQUEST:
            return {
                ...state,
                rebuildAppLoading: true,
                rebuildAppSuccess: false,
                rebuildAppError: null
            };
        case DokkuConstants.REBUILD_APP_SUCCESS:
            return {
                ...state,
                rebuildAppLoading: false,
                rebuildAppSuccess: true
            };
        case DokkuConstants.REBUILD_APP_FAILURE:
            return {
                ...state,
                rebuildAppLoading: false,
                rebuildAppSuccess: false,
                rebuildAppError: action.payload
            };

        case DokkuConstants.DELETE_APP_REQUEST:
            return {
                ...state,
                deleteAppLoading: true,
                deleteAppSuccess: false,
                deleteAppError: null
            };
        case DokkuConstants.DELETE_APP_SUCCESS:
            return {
                ...state,
                deleteAppLoading: false,
                deleteAppSuccess: true
            };
        case DokkuConstants.DELETE_APP_FAILURE:
            return {
                ...state,
                deleteAppLoading: false,
                deleteAppSuccess: false,
                deleteAppError: action.payload
            };


        case DokkuConstants.ADD_DOMAIN_REQUEST:
            return {
                ...state,
                addDomainLoading: true,
                addDomainSuccess: false,
                addDomainError: null
            };
        case DokkuConstants.ADD_DOMAIN_SUCCESS:
            return {
                ...state,
                addDomainLoading: false,
                addDomainSuccess: true
            };
        case DokkuConstants.ADD_DOMAIN_FAILURE:
            return {
                ...state,
                addDomainLoading: false,
                addDomainSuccess: false,
                addDomainError: action.payload
            };
        case DokkuConstants.DELETE_DOMAIN_REQUEST:
            return {
                ...state,
                deleteDomainLoading: true,
                deleteDomainSuccess: false,
                deleteDomainError: null
            };
        case DokkuConstants.DELETE_DOMAIN_SUCCESS:
            return {
                ...state,
                deleteDomainLoading: false,
                deleteDomainSuccess: true
            };
        case DokkuConstants.DELETE_DOMAIN_FAILURE:
            return {
                ...state,
                deleteDomainLoading: false,
                deleteDomainSuccess: false,
                deleteDomainError: action.payload
            };


        case DokkuConstants.ADD_PORT_REQUEST:
            return {
                ...state,
                addPortLoading: true,
                addPortSuccess: false,
                addPortError: null
            };
        case DokkuConstants.ADD_PORT_SUCCESS:
            return {
                ...state,
                addPortLoading: false,
                addPortSuccess: true
            };
        case DokkuConstants.ADD_PORT_FAILURE:
            return {
                ...state,
                addPortLoading: false,
                addPortSuccess: false,
                addPortError: action.payload
            };
        case DokkuConstants.DELETE_PORT_REQUEST:
            return {
                ...state,
                deletePortLoading: true,
                deletePortSuccess: false,
                deletePortError: null
            };
        case DokkuConstants.DELETE_PORT_SUCCESS:
            return {
                ...state,
                deletePortLoading: false,
                deletePortSuccess: true
            };
        case DokkuConstants.DELETE_PORT_FAILURE:
            return {
                ...state,
                deletePortLoading: false,
                deletePortSuccess: false,
                deletePortError: action.payload
            };


        case DokkuConstants.ADD_STORAGE_REQUEST:
            return {
                ...state,
                addStorageLoading: true,
                addStorageSuccess: false,
                addStorageError: null
            };
        case DokkuConstants.ADD_STORAGE_SUCCESS:
            return {
                ...state,
                addStorageLoading: false,
                addStorageSuccess: true
            };
        case DokkuConstants.ADD_STORAGE_FAILURE:
            return {
                ...state,
                addStorageLoading: false,
                addStorageSuccess: false,
                addStorageError: action.payload
            };
        case DokkuConstants.DELETE_STORAGE_REQUEST:
            return {
                ...state,
                deleteStorageLoading: true,
                deleteStorageSuccess: false,
                deleteStorageError: null
            };
        case DokkuConstants.DELETE_STORAGE_SUCCESS:
            return {
                ...state,
                deleteStorageLoading: false,
                deleteStorageSuccess: true
            };
        case DokkuConstants.DELETE_STORAGE_FAILURE:
            return {
                ...state,
                deleteStorageLoading: false,
                deleteStorageSuccess: false,
                deleteStorageError: action.payload
            };


        case DokkuConstants.UPDATE_STATUS_REQUEST:
            return {
                ...state,
                updateStatusLoading: true,
                updateStatusSuccess: false,
                updateStatusError: null
            };
        case DokkuConstants.UPDATE_STATUS_SUCCESS:
            return {
                ...state,
                updateStatusLoading: false,
                updateStatusSuccess: true
            };
        case DokkuConstants.UPDATE_STATUS_FAILURE:
            return {
                ...state,
                updateStatusLoading: false,
                updateStatusSuccess: false,
                updateStatusError: action.payload
            };

        case DokkuConstants.UPDATE_STATUS_REPORT_REQUEST:
            return {
                ...state,
                updateStatusReportLoading: true,
                updateStatusReportSuccess: false,
                updateStatusReportError: null
            };
        case DokkuConstants.UPDATE_STATUS_REPORT_SUCCESS:
            return {
                ...state,
                updateStatusReportLoading: false,
                updateStatusReportSuccess: true
            };
        case DokkuConstants.UPDATE_STATUS_REPORT_FAILURE:
            return {
                ...state,
                updateStatusReportLoading: false,
                updateStatusReportSuccess: false,
                updateStatusReportError: action.payload
            };

        default:
            return state
    }
}
