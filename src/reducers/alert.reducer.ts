import {AlertConstants} from "../constants/alert.constants";

export type AlertState = {
    alertType?: string;
    alertMessage?: string;
}

const initialState: AlertState = {};

export function alert(state = initialState, action): AlertState {
    switch (action.type) {
        case AlertConstants.SUCCESS:
            return {
                alertType: 'alert-success',
                alertMessage: action.message
            };
        case AlertConstants.ERROR:
            return {
                alertType: 'alert-danger',
                alertMessage: action.message
            };
        case AlertConstants.CLEAR:
            return {};
        default:
            return state
    }
}