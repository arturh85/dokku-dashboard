export const cleanupOutput = (output: string, skip: number = 1): string[] => {
    if (!output) {
        return null;
    }
    return output.split('\n').slice(skip).filter(a => a).map(s => s.trim())
};
export const colonLinesToObject = (output: string, skip: number = 1) => {
    if (!output) {
        return null;
    }
    return cleanupOutput(output, skip).reduce((obj, line) => {
        const index = line.indexOf(':');
        if (index === -1) {
            return obj;
        }
        const key = line.substr(0, index).trim();
        obj[key] = line.substr(index + 1).trim();
        return obj;
    }, <Object>{});
};

export function parseTabledOutput(output: string, columns: string[], skip: number = 3): Object[] {
    if (!output) {
        return null;
    }
    const lines = cleanupOutput(output, skip);
    return lines.map(line => {
        const reducedLine = line.trim()
            .replace(/  /g, '§') // detect fields by two ore more spaces
            .replace(/ +(?= )/g, '') // remote superfluous spaces
            .replace(/§+(?=§)/g, ''); // remote superfluous §
        const parts: string[] = reducedLine.split('§').map(s => s.trim());
        return columns.reduce((obj, column, index) => {
            obj[column] = parts[index];
            return obj;
        }, {});
    });
}

export function escapeRegExp(string) {
    return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
}