﻿// import * as apicache from 'apicache'
import {DokkuBackendService} from "./dokkuBackendService";
import {GitlabBackendService} from "./gitlabBackendService";
import {PERMISSION_ADD_APP} from "../../models/api.model";
import * as userService from '../users/user.service';

const express = require('express');
const router = express.Router();

const dokkuBackendService: DokkuBackendService = require('./dokkuBackendService');
const gitlabBackendService: GitlabBackendService = require('./gitlabBackendService');
// let cache = apicache.middleware;

// routes
router.post('/dokku-status', dokkuStatus);
router.post('/list-dokku-templates', listDokkuTemplates);
router.post('/update-status', updateStatus);
router.post('/load-logs', loadLogs);
router.post('/update-status-report', updateStatusReport);
router.post('/letsencrypt', letsencrypt);
router.post('/dokku-unlock-app', unlockApp);
router.post('/dokku-add-app', addApp);
router.post('/dokku-rebuild-app', rebuildApp);
router.post('/dokku-delete-app', deleteApp);
router.post('/dokku-add-domain', addDomain);
router.post('/dokku-delete-domain', deleteDomain);
router.post('/dokku-add-storage', addStorage);
router.post('/dokku-delete-storage', deleteStorage);
router.post('/dokku-add-port', addPort);
router.post('/dokku-delete-port', deletePort);

module.exports = router;

// const secureSuffix = (unsafeSuffix: string) => {
//     return path.normalize(unsafeSuffix).replace(/^(\.\.[\/\\])+/, '');
// };
//
const errorHandler = (req, res) => {
    return (err => {
        console.log('failed to execute', req.url, err);
        return res.status(500).send({message: err.message});
    });
};

function dokkuStatus(req, res) {
    if (dokkuBackendService.statusReport) {
        res.send(dokkuBackendService.statusReport);
    } else {
        res.status(500).send({message: 'failed to load dokku status, see server logs'});
    }
}

function updateStatus(req, res) {
    dokkuBackendService.dokkuUpdateStatus(req.body.appName)
        .then(() => res.send('ok'))
        .catch(errorHandler(req, res));
}

function updateStatusReport(req, res) {
    dokkuBackendService.dokkuUpdateStatusReport()
        .then(() => res.send('ok'))
        .catch(errorHandler(req, res));
}

function letsencrypt(req, res) {
    dokkuBackendService.letsencrypt(req.body.appName)
        .then(output => res.send(output))
        .catch(errorHandler(req, res));
}

function rebuildApp(req, res) {
    dokkuBackendService.rebuildApp(req.body.appName)
        .then(() => setTimeout(() => {
            res.send('ok');
        }, 1000))
        .catch(errorHandler(req, res));
}

function addApp(req, res, next) {
    userService.getById(req.user.sub)
        .then(user => {
            if(user.permissions.indexOf(PERMISSION_ADD_APP) == -1) {
                throw 'Unauthorized';
            }
            dokkuBackendService.addApp(req.body.appName, req.body.tagName, req.body.templateName, req.user.sub)
                .then(() => setTimeout(() => {
                    res.send('ok');
                }, 1000))
                .catch(errorHandler(req, res));
        })
        .catch(err => next(err));
}

function deleteApp(req, res) {
    dokkuBackendService.deleteApp(req.body.appName)
        .then(() => setTimeout(() => {
            res.send('ok');
        }, 5000))
        .catch(errorHandler(req, res));
}

function loadLogs(req, res) {
    dokkuBackendService.appLogs(req.body.appName, req.body.logLines)
        .then(appLogs => res.send(appLogs))
        .catch(errorHandler(req, res));
}

function addDomain(req, res) {
    dokkuBackendService.addDomain(req.body.appName, req.body.domain)
        .then(() => setTimeout(() => {
            res.send('ok');
        }, 1000))
        .catch(errorHandler(req, res));
}

function deleteDomain(req, res) {
    dokkuBackendService.deleteDomain(req.body.appName, req.body.domain)
        .then(() => setTimeout(() => {
            res.send('ok');
        }, 1000))
        .catch(errorHandler(req, res));
}


function addPort(req, res) {
    dokkuBackendService.addPortMapping(req.body.appName, req.body.portMapping)
        .then(() => setTimeout(() => {
            res.send('ok');
        }, 1000))
        .catch(errorHandler(req, res));
}

function deletePort(req, res) {
    dokkuBackendService.deletePortMapping(req.body.appName, req.body.portMapping)
        .then(() => setTimeout(() => {
            res.send('ok');
        }, 1000))
        .catch(errorHandler(req, res));
}


function addStorage(req, res) {
    dokkuBackendService.addStorageMount(req.body.appName, req.body.storageMount)
        .then(() => setTimeout(() => {
            res.send('ok');
        }, 1000))
        .catch(errorHandler(req, res));
}

function deleteStorage(req, res) {
    dokkuBackendService.deleteStorageMount(req.body.appName, req.body.storageMount)
        .then(() => setTimeout(() => {
            res.send('ok');
        }, 1000))
        .catch(errorHandler(req, res));
}

function unlockApp(req, res) {
    dokkuBackendService.unlockApp(req.body.appName)
        .then(() => setTimeout(() => {
            res.send('ok');
        }, 2000))
        .catch(errorHandler(req, res));
}


function listDokkuTemplates(req, res) {
    gitlabBackendService.listDokkuTemplates()
        .then(dokkuTemplates => res.send(dokkuTemplates))
        .catch(errorHandler(req, res));
}
