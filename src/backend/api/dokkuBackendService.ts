import * as child_process from "child_process";
import {ExecOptions} from "child_process";
import * as async from 'promise-async'
import {cleanupOutput, colonLinesToObject, parseTabledOutput} from "./utils.api";
import {
    AppConfig,
    AppDomains, AppLetsEncrypt,
    AppReport,
    AppStatus,
    AppStatusReport,
    ProxyPortsEntry,
    TagsEntry
} from "../../models/dokku.model.";
import {GitlabBackendService} from "./gitlabBackendService";


export class DokkuBackendService {
    statusReport: AppStatusReport = {};

    constructor(private gitlabBackendService: GitlabBackendService) {
        this.dokkuUpdateStatusReport()
            .then(() => {
                console.log('Startup status update successfull.');
            })
            .catch(err => {
                console.log('failed to fetch dokku status on startup', err);
            });
    }

    addApp(appName: string, tagName: string, templateName: string, userId: number): Promise<string> {
        return new Promise<string>((resolve, reject) => {
            this.dokkuExecute('apps:create ' + appName, true)
                .then(output1 => {
                    this.dokkuExecute(`config:set ${appName} DOKKU_TEMPLATE=${templateName} GITLAB_PROJECTNAME=${tagName}`, true)
                        .then(output1 => {
                            this.dokkuExecute('proxy:ports-add ' + appName + ' http:80:80 https:443:443', true)
                                .then(output2 => {
                                    this.dokkuUpdateStatus(appName).then(() => {
                                        if (templateName) {
                                            this.gitlabBackendService.initializeFromTemplate(appName, tagName, templateName, userId).then(output3 => {
                                                resolve(output1 + output2 + output3);
                                            }).catch(err => reject(err));
                                        } else {
                                            resolve();
                                        }
                                    });
                                })
                        })
                })
                .catch(err => reject(err));
        })
    }

    rebuildApp(appName: string) {
        return new Promise<string[]>((resolve, reject) => {
            this.dokkuExecute('ps:rebuild ' + appName, true)
                .then(() => {
                    this.dokkuUpdateStatus(appName).then(() => {
                        resolve()
                    });
                })
                .catch(err => reject(err));
        })
    }

    appLogs(appName: string, logLines: number): Promise<string> {
        return new Promise<string>((resolve, reject) => {
            this.dokkuExecute('logs ' + appName + ' -n ' + logLines, false)
                .then(appLogs => {
                    resolve(appLogs);
                })
                .catch(err => reject(err));
        })
    }

    deleteApp(appName: string) {
        return new Promise<string[]>((resolve, reject) => {
            this.dokkuExecute('apps:destroy ' + appName, true, appName)
                .then(() => {
                    delete this.statusReport[appName];
                    resolve();
                })
                .catch(err => reject(err));
        })
    }

    addDomain(appName: string, domain: string) {
        return new Promise<string[]>((resolve, reject) => {
            this.dokkuExecute('domains:add ' + appName + ' ' + domain, true)
                .then(() => {
                    this.dokkuUpdateStatus(appName).then(() => {
                        resolve()
                    });
                })
                .catch(err => reject(err));
        })
    }

    deleteDomain(appName: string, domain: string) {
        return new Promise<string[]>((resolve, reject) => {
            this.dokkuExecute('domains:remove ' + appName + ' ' + domain, true)
                .then(() => {
                    this.dokkuUpdateStatus(appName).then(() => {
                        resolve()
                    });
                })
                .catch(err => reject(err));
        })
    }

    addPortMapping(appName: string, portMapping: string) {
        return new Promise<string[]>((resolve, reject) => {
            this.dokkuExecute('proxy:ports-add ' + appName + ' ' + portMapping, true)
                .then(() => {
                    this.dokkuUpdateStatus(appName).then(() => {
                        resolve()
                    });
                })
                .catch(err => reject(err));
        })
    }

    deletePortMapping(appName: string, portMapping: string) {
        return new Promise<string[]>((resolve, reject) => {
            this.dokkuExecute('proxy:ports-remove ' + appName + ' ' + portMapping, true)
                .then(() => {
                    this.dokkuUpdateStatus(appName).then(() => {
                        resolve()
                    });
                })
                .catch(err => reject(err));
        })
    }

    addStorageMount(appName: string, storageMount: string) {
        return new Promise<string[]>((resolve, reject) => {
            this.dokkuExecute('storage:mount ' + appName + ' ' + storageMount, true)
                .then(() => {
                    this.dokkuUpdateStatus(appName).then(() => {
                        resolve()
                    });
                })
                .catch(err => reject(err));
        })
    }

    deleteStorageMount(appName: string, storageMount: string) {
        return new Promise<string[]>((resolve, reject) => {
            this.dokkuExecute('storage:unmount ' + appName + ' ' + storageMount, true)
                .then(() => {
                    this.dokkuUpdateStatus(appName).then(() => {
                        resolve()
                    });
                })
                .catch(err => reject(err));
        })
    }


    unlockApp(appName: string) {
        return new Promise<string[]>((resolve, reject) => {
            this.dokkuExecute('apps:unlock ' + appName, true)
                .then(() => {
                    this.dokkuUpdateStatus(appName).then(() => {
                        resolve()
                    });
                })
                .catch(err => reject(err));
        })
    }

    letsencrypt(appName: string): Promise<string> {
        return new Promise<string>((resolve, reject) => {
            this.dokkuExecute('letsencrypt ' + appName, true)
                .then(output => {
                    this.dokkuUpdateStatus(appName).then(() => {
                        resolve(output)
                    });
                })
                .catch(err => reject(err));
        })
    }

    dokkuUpdateStatus(appName: string): Promise<AppStatus> {
        return this.dokkuLetsEncryptLs().then(letsEncryptByAppName => {
            return Promise.all([
                this.dokkuAppsReport(appName),
                this.dokkuDomainsReport(appName),
                this.dokkuConfig(appName),
                this.dokkuStorageList(appName),
                this.dokkuProxyPorts(appName),
            ]).then(reports => {
                const status = {
                    appReport: reports[0],
                    domains: reports[1],
                    config: reports[2],
                    storage: reports[3],
                    proxyPorts: reports[4],
                    letsEncrypt: letsEncryptByAppName[appName]
                };
                this.statusReport[appName] = status;
                return status;
            });
        })

    }

    dokkuUpdateStatusReport(): Promise<AppStatusReport> {
        this.statusReport = {};
        const requestLimit = 1;
        return this.dokkuAppsList().then(apps => {
            return async.mapLimit(apps, requestLimit, (appName, callback) => {
                this.dokkuUpdateStatus(appName).then(status => callback(null, status));
            })
        });
    }

    dokkuAppsList(): Promise<string[]> {
        return new Promise<string[]>((resolve, reject) => {
            this.dokkuExecute('apps:list')
                .then(output => resolve(cleanupOutput(output)))
                .catch(err => reject(err));
        })
    }

    dokkuUrls(app: string): Promise<string[]> {
        return new Promise<string[]>((resolve, reject) => {
            this.dokkuExecute('urls ' + app)
                .then(output => resolve(cleanupOutput(output)))
                .catch(err => reject(err));
        })
    }

    dokkuAppsReport(app: string): Promise<AppReport> {
        return new Promise<AppReport>((resolve, reject) => {
            this.dokkuExecute('apps:report ' + app)
                .then(output => resolve(<AppReport>colonLinesToObject(output)))
                .catch(err => reject(err));
        })
    }

    dokkuDomainsReport(app: string): Promise<AppDomains> {
        return new Promise<AppDomains>((resolve, reject) => {
            this.dokkuExecute('domains:report ' + app)
                .then(output => {
                    if (!output) {
                        resolve(null);
                    }
                    const domains = colonLinesToObject(output);
                    domains['Domains app vhosts'] = domains['Domains app vhosts'].split(' ').filter(a => a);
                    resolve(<AppDomains>domains)
                })
                .catch(err => reject(err));
        })
    }

    dokkuLetsEncryptLs(): Promise<{[appName: string]: AppLetsEncrypt}> {
        return new Promise<{[appName: string]: AppLetsEncrypt}>((resolve, reject) => {
            this.dokkuExecute('letsencrypt:ls')
                .then(output => {
                    if (!output) {
                        resolve(null);
                    }
                    const letsencrypt = parseTabledOutput(output,
                        [
                            'App name',
                            'Certificate Expiry',
                            'Time before expiry',
                            'Time before renewal'
                        ],
                        1
                    );
                    const letsencryptByAppName = {};
                    letsencrypt.forEach(data => {
                        const appName = data['App name'];
                        delete data['App name'];
                        letsencryptByAppName[appName] = data;
                    });
                    resolve(letsencryptByAppName)
                })
                .catch(err => reject(err));
        })
    }

    dokkuConfig(app: string): Promise<AppConfig> {
        return new Promise<AppConfig>((resolve, reject) => {
            this.dokkuExecute('config ' + app)
                .then(output => resolve(<AppConfig>colonLinesToObject(output)))
                .catch(err => reject(err));
        })
    }

    dokkuStorageList(app: string): Promise<string[]> {
        return new Promise<string[]>((resolve, reject) => {
            this.dokkuExecute('storage:list ' + app)
                .then(output => resolve(cleanupOutput(output)))
                .catch(err => reject(err));
        })
    }

    dokkuProxyPorts(app: string): Promise<ProxyPortsEntry[]> {
        return new Promise<ProxyPortsEntry[]>((resolve, reject) => {
            this.dokkuExecute('proxy:ports ' + app)
                .then(output => resolve(<ProxyPortsEntry[]>
                    parseTabledOutput(output, ['scheme', 'hostPort', 'containerPort'], 2)))
                .catch(err => resolve(err));
        })
    }

    dokkuTags(app: string): Promise<TagsEntry[]> {
        return new Promise<TagsEntry[]>((resolve, reject) => {
            this.dokkuExecute('proxy:ports ' + app)
                .then(output => resolve(<TagsEntry[]>
                    parseTabledOutput(output, ['repository',
                        'tag',
                        'imageId',
                        'created',
                        'size',], 2)))
                .catch(err => reject(err));
        })
    }

    dokkuExecute(dokkuCommand: string, log: boolean = false, stdinInput: string = null): Promise<null | string> {
        const timeout: number = 30000;
        const dokkuUser = 'dokku';
        const dokkuHost = process.env.DOKKU_HOST;
        const dokkuPort = process.env.DOKKU_PORT || 22;
        const command = 'ssh -p ' + dokkuPort + ' ' + dokkuUser + '@' + dokkuHost + ' -- ' + dokkuCommand;
        if (log) {
            console.log('running', command);
        }
        try {
            const options: ExecOptions = {
                timeout,
                maxBuffer: 1024 * 1024 // 1 MB
            };
            return new Promise<string>((resolve, reject) => {
                    const cp = child_process.exec(command, options, (err, buffer) => {
                        if (err) {
                            console.log('failed to run', command, err.message);
                            resolve(null);
                        } else {
                            const output = buffer.toString();
                            if (log) {
                                console.log('output', output);
                            }
                            resolve(output);
                        }
                    });
                    if (stdinInput) {
                        cp.stdin.write(stdinInput + "\n");
                    }
                }
            );
        } catch (err) {
            console.log('failed to run', command, err);
            return Promise.resolve(null);
        }
    }
}

const gitlabBackendService = require('./gitlabBackendService');
const instance = new DokkuBackendService(gitlabBackendService);
module.exports = instance;
