import * as request from 'request';
import {SentryCreateProjectResponse, SentryListClientKeysResponse} from "../../models/sentry.model.";

export class SentryBackendService {
    addProject(sentryProjectName: string): Promise<SentryCreateProjectResponse> {
        return new Promise<SentryCreateProjectResponse>((resolve, reject) => {
            if (!process.env.SENTRY_API_URL || !process.env.SENTRY_ACCESS_TOKEN ||
                !process.env.SENTRY_ORGANIZATION || !process.env.SENTRY_TEAM) {
                console.log('ERROR: missing environment variables for sentry');
                return resolve(null);
            }
            request({
                url: `${process.env.SENTRY_API_URL}/api/0/teams/${process.env.SENTRY_ORGANIZATION}/${process.env.SENTRY_TEAM}/projects`,
                json: true,
                method: 'POST',
                headers: {
                    Authorization: `Bearer ${process.env.SENTRY_ACCESS_TOKEN}`
                },
                body: {
                    name: sentryProjectName
                }
            }, (err, response, body) => {
                if (err) {
                    return reject(err);
                }
                resolve(body);
            });
        });
    }
    deleteProject(sentryProjectSlug: string): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            if (!process.env.SENTRY_API_URL || !process.env.SENTRY_ACCESS_TOKEN ||
                !process.env.SENTRY_ORGANIZATION || !process.env.SENTRY_TEAM) {
                console.log('ERROR: missing environment variables for sentry');
                return resolve(null);
            }
            request({
                url: `${process.env.SENTRY_API_URL}/api/0/projects/${process.env.SENTRY_ORGANIZATION}/${sentryProjectSlug}/`,
                json: true,
                method: 'DELETE',
                headers: {
                    Authorization: `Bearer ${process.env.SENTRY_ACCESS_TOKEN}`
                },
            }, (err, response, body) => {
                if (err) {
                    return reject(err);
                }
                console.log('sentry delete project response', body);
                resolve();
            });
        });
    }
    getClientKeys(sentryProjectSlug: string): Promise<SentryListClientKeysResponse[]> {
        return new Promise<SentryListClientKeysResponse[]>((resolve, reject) => {
            if (!process.env.SENTRY_API_URL || !process.env.SENTRY_ACCESS_TOKEN ||
                !process.env.SENTRY_ORGANIZATION || !process.env.SENTRY_TEAM) {
                console.log('ERROR: missing environment variables for sentry');
                return resolve(null);
            }
            request({
                url: `${process.env.SENTRY_API_URL}/api/0/projects/${process.env.SENTRY_ORGANIZATION}/${sentryProjectSlug}/keys/`,
                json: true,
                method: 'GET',
                headers: {
                    Authorization: `Bearer ${process.env.SENTRY_ACCESS_TOKEN}`
                },
            }, (err, response, body) => {
                if (err) {
                    return reject(err);
                }
                resolve(body);
            });
        });
    }
}

const instance = new SentryBackendService();
module.exports = instance;
