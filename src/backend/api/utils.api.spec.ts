import {cleanupOutput, colonLinesToObject, parseTabledOutput} from "./utils.api";

describe('api utils', () => {
    describe('cleanupOutput', () => {
        it('cleanup sample', () => {
            const sample = '=====> Image tags for dokku/app42.blue\n' +
                'REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE\n' +
                'dokku/app42.blue    latest              320cfd16c0a1        23 hours ago        18.6MB\n'
            expect(cleanupOutput(sample, 1)).toEqual([
                'REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE',
                'dokku/app42.blue    latest              320cfd16c0a1        23 hours ago        18.6MB'
            ]);
        });
    });

    describe('parseTabledOutput', () => {
        it('parses sample', () => {
            const sample = '=====> Image tags for dokku/app42.blue\n' +
                'REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE\n' +
                'dokku/app42.blue    latest              320cfd16c0a1        23 hours ago        18.6MB\n';
            expect(parseTabledOutput(sample, [
                'repository',
                'tag',
                'imageId',
                'created',
                'size',], 2)).toEqual([{
                repository: 'dokku/app42.blue',
                tag: 'latest',
                imageId: '320cfd16c0a1',
                created: '23 hours ago',
                size: '18.6MB',
            }]);
        });
    });
    describe('colonLinesToObject', () => {
        it('parses sample', () => {
            const sample = '=====> app42.blue app information\n' +
                '       App dir:                       /home/dokku/app42.blue\n' +
                '       Git sha:                       bdf8738\n' +
                '       invalid_data\n' +
                '       Deploy source:                 git\n' +
                '       Locked:                        false\n'
            expect(colonLinesToObject(sample, 1)).toEqual({
                'App dir': '/home/dokku/app42.blue',
                'Git sha': 'bdf8738',
                'Deploy source': 'git',
                'Locked': 'false',
            });
        });
    });
});
