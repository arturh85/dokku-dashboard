import * as request from 'request';
import * as fs from 'fs';
import * as path from 'path';
import * as glob from 'glob';
import {escapeRegExp} from "./utils.api";
import * as os from "os";
import * as child_process from "child_process";
import {deleteDirectory} from "../util";
import {userDatabase} from "../users/user.service";

export type DokkuTemplate = {
    name: string;
    ssh_url_to_repo: string;
    avatar_url: string;
    description: string;
};

export class GitlabBackendService {
    listDokkuTemplates(): Promise<DokkuTemplate[]> {
        return new Promise<DokkuTemplate[]>((resolve, reject) => {
            if (!process.env.GITLAB_API_URL || !process.env.GITLAB_ACCESS_TOKEN || !process.env.GITLAB_TEMPLATE_NAMESPACE) {
                console.log('ERROR: missing environment variables for gitlab');
                return resolve([]);
            }
            request({
                url: process.env.GITLAB_API_URL + 'groups/6/projects?private_token=' + process.env.GITLAB_ACCESS_TOKEN + '&simple=true',
                json: true
            }, (err, response, body) => {
                if (err) {
                    return reject(err);
                }
                const dokkuTemplateProject = body.filter(project => project.path_with_namespace.indexOf(process.env.GITLAB_TEMPLATE_NAMESPACE + '/') === 0);
                resolve(dokkuTemplateProject.map(project => {
                    return {
                        name: project.name,
                        ssh_url_to_repo: project.ssh_url_to_repo,
                        avatar_url: project.avatar_url,
                        description: project.description
                    };
                }));
            });
        });
    }

    getProjectGitUrl(path_with_namespace: string): Promise<string> {
        return new Promise<string>((resolve, reject) => {
            if (!process.env.GITLAB_API_URL || !process.env.GITLAB_ACCESS_TOKEN || !process.env.GITLAB_TEMPLATE_NAMESPACE) {
                console.log('ERROR: missing environment variables for gitlab');
                return reject(null);
            }
            request({
                url: process.env.GITLAB_API_URL + 'projects?private_token=' + process.env.GITLAB_ACCESS_TOKEN + '&simple=true',
                json: true
            }, (err, response, body) => {
                if (err) {
                    return reject(err);
                }
                const project = body.find(project => project.path_with_namespace === path_with_namespace);
                if (project) {
                    resolve(project.ssh_url_to_repo);
                } else {
                    reject(new Error('failed to find project ' + path_with_namespace))
                }
            });
        });
    }

    templateNamespace(): Promise<number> {
        return new Promise<number>((resolve, reject) => {
            if (!process.env.GITLAB_API_URL || !process.env.GITLAB_ACCESS_TOKEN || !process.env.GITLAB_TEMPLATE_NAMESPACE) {
                console.log('ERROR: missing environment variables for gitlab');
                return reject(null);
            }
            request({
                url: process.env.GITLAB_API_URL + 'namespaces?private_token=' + process.env.GITLAB_ACCESS_TOKEN,
                json: true
            }, (err, response, body) => {
                if (err) {
                    return reject(err);
                }
                const dokkuTemplateNamespace = body.filter(ns => ns.name === process.env.GITLAB_TEMPLATE_NAMESPACE);
                if (dokkuTemplateNamespace) {
                    resolve(dokkuTemplateNamespace.id);
                } else {
                    reject(new Error("failed to find dokku template namespace " + process.env.GITLAB_TEMPLATE_NAMESPACE))
                }
            });
        });
    }

    dokkuAppNamespace(): Promise<number> {
        return new Promise<number>((resolve, reject) => {
            if (!process.env.GITLAB_API_URL || !process.env.GITLAB_ACCESS_TOKEN || !process.env.GITLAB_DOKKU_APP_NAMESPACE) {
                console.log('ERROR: missing environment variables for gitlab');
                return reject(null);
            }
            request({
                url: process.env.GITLAB_API_URL + 'namespaces?private_token=' + process.env.GITLAB_ACCESS_TOKEN,
                json: true
            }, (err, response, body) => {
                if (err) {
                    return reject(err);
                }
                const dokkuAppNamespace = body.find(ns => ns.name === process.env.GITLAB_DOKKU_APP_NAMESPACE);
                if (dokkuAppNamespace) {
                    resolve(dokkuAppNamespace.id);
                } else {
                    reject(new Error("failed to find dokku app namespace " + process.env.GITLAB_DOKKU_APP_NAMESPACE))
                }
            });
        });
    }

    appDomain(appName: string) {
        return appName.indexOf('.') !== -1 ? appName : appName + '.' + process.env.DOKKU_DOMAIN
    }

    createGitlabProject(appName: string, tagName: string): Promise<string> {
        return this.dokkuAppNamespace().then(appNamespaceId => {
            return new Promise<string>((resolve, reject) => {
                if (!process.env.GITLAB_API_URL || !process.env.GITLAB_ACCESS_TOKEN || !process.env.DOKKU_DOMAIN) {
                    const msg = 'ERROR: missing environment variables for gitlab';
                    console.log(msg);
                    return reject(new Error(msg));
                }

                const domain = this.appDomain(appName);
                const appUrl = 'https://' + domain;
                const gitlabUrl = process.env.GITLAB_API_URL + 'projects?private_token=' + process.env.GITLAB_ACCESS_TOKEN
                    + '&name=' + encodeURIComponent(tagName)
                    + '&namespace_id=' + appNamespaceId
                    + '&description=' + encodeURIComponent(appUrl)
                    + '&visibility=internal'
                ;

                request({
                    url: gitlabUrl,
                    json: true,
                    method: 'POST',
                }, (err, response, body) => {
                    if (err) {
                        return reject(err);
                    }
                    if (!body.ssh_url_to_repo) {
                        return reject(new Error('failed'));
                    }
                    resolve(body.ssh_url_to_repo);
                });
            });
        });
    }

    findAndReplace(rootPath: string, searchReplace: { [search: string]: string }): Promise<string[]> {
        return new Promise<string[]>((resolve, reject) => {
            const globOptions = {nodir: true, dot: true};
            glob(path.join(rootPath, "**/*"), globOptions, function (err, files) {
                if (err) {
                    return reject(err);
                }
                let modifiedFiles = [];
                files.forEach(filePath => {
                    if (filePath.indexOf('.git/') !== -1) {
                        return;
                    }
                    let fileContent = fs.readFileSync(filePath, {encoding: 'utf8'});
                    let modified = false;
                    Object.keys(searchReplace).forEach(search => {
                        if (fileContent.indexOf(search) !== -1) {
                            fileContent = fileContent.replace(new RegExp(escapeRegExp(search), 'g'), searchReplace[search]);
                            modified = true;
                        }
                    });
                    if (modified) {
                        modifiedFiles.push(filePath.replace(rootPath, ''));
                        fs.writeFileSync(filePath, fileContent, {encoding: 'utf8'});
                    }
                });
                resolve(modifiedFiles);
            })
        });
    }

    renameFilesAndFolders(rootPath: string, searchReplace: { [search: string]: string }): Promise<string[]> {
        return new Promise<string[]>((resolve, reject) => {
            const globOptions = {nodir: true, dot: true};
            glob(path.join(rootPath, "**/*"), globOptions, (err, files) => {
                if (err) {
                    return reject(err);
                }
                let modifiedFiles = [];
                files.forEach(filePath => {
                    let modified = false;
                    let fileName = path.basename(filePath);
                    Object.keys(searchReplace).forEach(search => {
                        if (fileName.indexOf(search) !== -1) {
                            fileName = fileName.replace(new RegExp(escapeRegExp(search), 'g'), searchReplace[search]);
                            modified = true;
                        }
                    });
                    if (modified) {
                        const newPath = path.dirname(filePath) + '/' + fileName;
                        console.log('rename file', filePath, 'to', newPath);
                        // fs.renameSync(filePath, newPath);
                        this.runCommand('git mv ' + filePath.replace(rootPath.replace(/\\/g, '/') + '/', '') + ' ' + newPath.replace(rootPath.replace(/\\/g, '/') + '/', ''), rootPath);


                        modifiedFiles.push(newPath.replace(rootPath, ''));
                    }
                });

                const globOptions = {nodir: false, dot: true};
                glob(path.join(rootPath, "**/*/"), globOptions, (err, files) => {
                    if (err) {
                        return reject(err);
                    }
                    files.forEach(filePath => {
                        let modified = false;
                        let dirName = path.basename(filePath);
                        Object.keys(searchReplace).forEach(search => {
                            if (dirName.indexOf(search) !== -1) {
                                dirName = dirName.replace(new RegExp(escapeRegExp(search), 'g'), searchReplace[search]);
                                modified = true;
                            }
                        });
                        if (modified) {
                            const newPath = path.dirname(filePath) + '/' + dirName;
                            console.log('rename dir ', filePath, 'to', newPath);
                            this.runCommand('git mv ' + filePath.replace(rootPath.replace(/\\/g, '/') + '/', '') + ' ' + newPath.replace(rootPath.replace(/\\/g, '/') + '/', ''), rootPath);
                            modifiedFiles.push(newPath.replace(rootPath, ''));
                        }
                    });
                    resolve(modifiedFiles);
                })

            })
        });
    }

    runCommand(command, cwd) {
        console.log('running', command);
        const commandOutput = child_process.execSync(command, {cwd: cwd}).toString();
        console.log(commandOutput);
        return 'running ' + command + ' => ' + commandOutput;
    };

    initializeFromTemplate(appName: string, tagName: string, templateName: string, userId: number): Promise<string> {
        return new Promise<string>((resolve, reject) => {
            const tmpDir = os.tmpdir();
            const tmpWorkspace = path.join(tmpDir, 'dokkuDashboard');
            const tmpPath = path.join(tmpWorkspace, appName);
            if (!fs.existsSync(tmpWorkspace)) {
                fs.mkdirSync(tmpWorkspace);
            }
            // gitlab create project if not exists
            let initOutput = '';
            const runCommand = (command, cwd = tmpPath) => {
                initOutput += this.runCommand(command, cwd)
            };
            const ucwords = (str) => {
                return (str + '').replace(/^(.)|\s+(.)/g, function ($1) {
                    return $1.toUpperCase();
                })
            };
            this.listDokkuTemplates().then(templates => {
                const template = templates.find(t => t.name === templateName);
                this.createGitlabProject(appName, tagName).then(gitUrl => {
                    const doStuff = () => {
                        try {
                            // git clone template.ssh_url_to_repo
                            if (!fs.existsSync(tmpPath)) {
                                runCommand('git clone "' + template.ssh_url_to_repo + '" ' + tmpPath, tmpDir);
                            }
                            if (!fs.existsSync(tmpPath)) {
                                console.log('failed to find checked out path');
                                return reject(null)
                            }
                            const domain = this.appDomain(appName);
                            const appUrl = 'https://' + domain;
                            const className = ucwords(tagName.replace(/-/g, ' '))
                                .replace(/ /g, '');

                            const replacements = {
                                DOKKUTEMPLATE_APP_NAME: appName,
                                DOKKUTEMPLATE_CLASS_NAME: className,
                                DOKKUTEMPLATE_TAG_NAME: tagName,
                                DOKKUTEMPLATE_APP_URL: appUrl,
                                '.REMOVEDOT': ''
                            };
                            // sed-recursive
                            this.renameFilesAndFolders(tmpPath, replacements).then(() => {
                                this.findAndReplace(tmpPath, replacements).then(() => {
                                    // git commit
                                    const user = userDatabase.find(user => user.id === userId);
                                    runCommand(`git config --global user.name "${user.name}"`);
                                    runCommand(`git config --global user.email "${user.email}"`);
                                    runCommand('git add .');
                                    runCommand(`git commit -a -m "initialize ${appName} from template ${templateName}"`);

                                    // git remote set-url origin gitlab-ssh
                                    runCommand('git remote set-url origin "' + gitUrl + '"');

                                    // git push origin
                                    runCommand('git push origin master');

                                    deleteDirectory(tmpPath).then(() => {
                                        resolve(initOutput);
                                    }).catch(err => reject(err));
                                });
                            });
                        } catch (err) {
                            console.log('failure running commands', err);
                            console.log('err.output.toString()', err.output.toString());
                            console.log('err.stdout.toString()', err.stdout.toString());
                            console.log('err.stderr.toString()', err.stderr.toString());
                            reject(err);
                        }
                    };

                    if (fs.existsSync(tmpPath)) {
                        deleteDirectory(tmpPath).then(() => {
                            doStuff();
                        }).catch(err => reject(err))
                    } else {
                        doStuff();
                    }

                }).catch(err => reject(err))
            }).catch(err => reject(err))
        });
    }
}

const instance = new GitlabBackendService();
module.exports = instance;
