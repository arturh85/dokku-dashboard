﻿import {User} from '../../models/user.model';
import {allPermissions} from "../../models/api.model";

const secret = "7298185137150880975538559878819713681284680177490713882885487896493319834070842121192792139419472447";
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');

export const permissionsByGroup = {
    admin: allPermissions,
    user: allPermissions.slice(0, allPermissions.length - 1), // all except write mounts
    guest: []
};

export const userDatabase: User[] = [
    {
        id: 1,
        username: 'guest',
        name: 'Guest',
        email: 'guest@arturh.de',
        hash: bcrypt.hashSync('guest', 10),
        createdDate: new Date().toString(),
        permissions: permissionsByGroup['guest']
    }
];

export async function authenticate({username, password}) {
    const user = userDatabase.find(user => user.username === username);
    if (user && bcrypt.compareSync(password, user.hash)) {
        const {hash, ...userWithoutHash} = user;
        const token = jwt.sign({sub: user.id}, secret);
        return {
            ...userWithoutHash,
            token
        };
    }
}

export async function getAll() {
    return userDatabase;
}

export async function getById(id) {
    return userDatabase.find(user => user.id === id);
}

// async function create(userParam) {
//   // validate
//   if (await User.findOne({username: userParam.username})) {
//     throw 'Username "' + userParam.username + '" is already taken';
//   }
//
//   const user = new User(userParam);
//
//   // hash password
//   if (userParam.password) {
//     user.hash = bcrypt.hashSync(userParam.password, 10);
//   }
//
//   // save user
//   await user.save();
// }

// async function update(id, userParam) {
//   const user = await User.findById(id);
//
//   // validate
//   if (!user) throw 'User not found';
//   if (user.username !== userParam.username && await User.findOne({username: userParam.username})) {
//     throw 'Username "' + userParam.username + '" is already taken';
//   }
//
//   // hash password if it was entered
//   if (userParam.password) {
//     userParam.hash = bcrypt.hashSync(userParam.password, 10);
//   }
//
//   // copy userParam properties to user
//   Object.assign(user, userParam);
//
//   await user.save();
// }
//
// async function _delete(id) {
//   await User.findByIdAndRemove(id);
// }
