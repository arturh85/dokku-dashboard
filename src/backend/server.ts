require('rootpath')();
import * as Bundler from 'parcel-bundler';
import * as serve from 'browser-sync';
import * as compress from 'compression';
import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as path from 'path';
import * as cors from 'cors';

const jwt = require('./helpers/jwt');
const errorHandler = require('./helpers/error-handler');
const app = express();

// app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(bodyParser.raw({type: 'application/octet-stream'}));
app.use(cors());
app.use(jwt());
app.use('/api', require('./api/api.controller'));
app.use('/users', require('./users/users.controller'));
app.get('/status', (req, res) => res.send('ok'));
app.use(errorHandler);
console.log('Environment:', app.get('env'));
const PORT = process.env.PORT || 5000;
if (app.get('env') === 'production') {
    app.use(express.static('dist/'))
    app.get(/[^\.]/, function (request, response) {
        response.sendFile(path.resolve(__dirname, '..', '..', 'dist', 'index.html'));
    });
    const server = require('http').Server(app);
    const io = require('socket.io')(server);
    io.on('connection', (socket) => {
        console.log('socket.io connected', socket.id);
        // handle(socket);
    });
    server.listen(PORT, () => console.log('🚀  Server ready at http://localhost:' + PORT));
} else {
    const bundler = new Bundler('public/index.html');
    const bs = serve({
        port: PORT,
        open: false,
        server: {baseDir: 'dist', https: false},
        socket: {
            namespace: '/mqtt-socket'
        },
        middleware: [
            app,
            compress(),
            bundler.middleware()
        ],
    });
    // handle(bs.sockets);
}
