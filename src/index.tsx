import * as React from "react";
import {render} from 'react-dom';
import {Redirect, Route, Router, Switch} from "react-router-dom";
import indexRoutes from "routes/index";
import "bootstrap/dist/css/bootstrap.min.css";
import "./assets/css/animate.min.css";
import "./assets/sass/light-bootstrap-dashboard.scss";
import "./assets/css/demo.css";
import "./assets/css/pe-icon-7-stroke.css";
import "./assets/css/diff.css";
import {Provider} from 'react-redux';
import {history, store} from './helpers';

render(
    <Provider store={store}>
        <Router history={history}>
            <Switch>
                {indexRoutes.map((prop, key) => {
                    return <Route to={prop.path} component={prop.component} key={key}/>;
                })}
            </Switch>
        </Router>
    </Provider>,
    document.getElementById("root")
);
