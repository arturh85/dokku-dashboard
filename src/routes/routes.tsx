import {LoginPage} from "../views/LoginPage/LoginPage";
import {OverviewPage} from "../views/OverviewPage/OverviewPage";
import {AppDetailPage} from "../views/AppDetailPage/AppDetailPage";
import {AppNewPage} from "../views/AppNewPage/AppNewPage";
import {PERMISSION_ADD_APP} from "../models/api.model";

let applicationRoutes: any = [
    {
        path: "/apps",
        name: "Overview",
        icon: "pe-7s-home",
        component: OverviewPage
    },
    {
        path: "/app/:appName",
        name: "App Detail",
        hideInMenu: true,
        component: AppDetailPage,
        permissionsRequired: [PERMISSION_ADD_APP]
    },
    {
        path: "/new",
        name: "New App",
        icon: "pe-7s-plus",
        component: AppNewPage
    },
    {
        path: "/login",
        name: "Login Page",
        component: LoginPage,
        public: true,
        hideInMenu: true
    },
    {redirect: true, path: "/", to: "/apps"}
];

export default applicationRoutes;
