**dokku-dashboard**

Dashboard for Dokku Servers

Features:
 - Beautiful Dashboard listing registered [Dokku](http://dokku.viewdocs.io/dokku/) Apps
 
Build with:
 - light-bootstrap-dashboard-react as base
   - see https://www.creative-tim.com/product/light-bootstrap-dashboard-react
 - React
 - Parcel for bundling
 - Jest for Testing

# Run as developer:
 
Lets start!

 1. Clone Repo

```
    git clone git@gitlab.app42.blue:dokku/status.git
```

 2. Install Dependencies
 
```
    yarn install or npm install
```

 3. Run!
 
 ```
    export DOKKU_HOST="my-server.tld"
    export DOKKU_PORT="2222"
    yarn start or npm run start
```

For this to work your shell needs to be able to connect to your dokku server via command line ssh. 
You can always contact me via arturh@arturh.de or report an issue.

# Deploy to Dokku:

 - create an ```id_rsa``` private key file authorized for dokku in the project root 
 - create an ```env.sh``` file in the project root with: 

 ```
    export DOKKU_HOST="my-server.tld"
    export DOKKU_PORT="22"
```
 - commit those files and push to your dokku as usual


# Gitlab Variables for AutoCI

- GITLAB_API_URL
- GITLAB_ACCESS_TOKEN
- NPM_REGISTRY
- DOKKU_HOST
- DOKKU_PORT
- DOKKU_DOMAIN
