FROM node:10
WORKDIR /app
COPY . /app
COPY CHECKS /app/CHECKS

ENV TZ=Europe/Berlin
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN chmod +x /app/build.sh
RUN /app/build.sh || exit 1

ENV NODE_ENV production
ENV PORT 80
EXPOSE 80
CMD . /app/env.sh && npm run start
